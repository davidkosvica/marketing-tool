<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;
use App\Model\Google\GoogleAuthorisation;
use App\Model\UserManager;

/**
 * Description of GoogleOauth2callback
 *
 * @author david
 */
class GoogleOauth2CallbackPresenter extends UserAreaPresenter  {
	
	private $googleAuth;
	private $userManager;

	public function __construct(GoogleAuthorisation $googleAuth,
		UserManager $userManager) {
		$this->googleAuth = $googleAuth;
		$this->userManager = $userManager;
	}

	public function startup() {
		$request = $this->getHttpRequest();
		$code = $request->getQuery('code');
		if (!is_null($code)) {
			$tokens = $this->googleAuth->getAccessToken($code);
			$accessToken = $tokens['access_token'];
			$refreshToken = $tokens['refresh_token'];
			$googleUserId = $this->googleAuth->getUserId($accessToken);
			$userId = $this->getUser()->getId();
			
			$exists = $this->userManager->FindByGoogleId($googleUserId);
			if ($exists) {
				$this->userManager->activateUsersGoogleAccount($userId, $googleUserId);
				$this->userManager->UpdateGoogleAccessToken($userId, $accessToken);
				if ($refreshToken) {
					$this->userManager->UpdateGoogleRefreshToken($userId, $refreshToken);
				}
			} else {
				$userObject = $this->googleAuth->getUserId($accessToken);
				$this->userManager->RegisterFromGoogle($userObject, $userId);
			}
		}
		$this->redirect('ConnectServices:');
	}
}
