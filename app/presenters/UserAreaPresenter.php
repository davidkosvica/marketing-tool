<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;
use App;

/**
 * Description of userAreaPresenter
 *
 * @author davidkosvica
 */
class UserAreaPresenter extends BasePresenter {
	
	/**
	 *  @var \App\Model\UserManager $usersManager; 
	 */
	protected $usersManager;
	
	public function __construct(App\Model\UserManager $usersManager) {
		parent::__construct();
		$this->usersManager = $usersManager;
	}
	
	protected function startup() {
		parent::startup();
		
		if (!$this->getUser()->isAllowed('ConnectServicesPresenter')) {
			$this->flashMessage("Nemáte opravnění.");
			$this->redirect('Homepage:');
		}
		
	}
}
