<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;
use Kdyby;
use App;
use Nette\Application\UI\Form;
use App\Forms\SignInFormFactorySklik;
use App\Model\Sklik\SklikAdAccounts;
use App\Model\Google\GoogleAuthorisation;
use App\Components\ConnectingServices\connectGoogle;

/**
 * Description of FacebookLogin
 *
 * @author davidkosvica
 */
class ConnectServicesPresenter extends UserAreaPresenter {
	
	/** @var \Kdyby\Facebook\Facebook */
	private $facebook;
	
	/** @var \Kdyby\Google\Google */
	private $google;
	
	/** @var \App\Forms\SignInFormFactorySklik */
	private $sklikFormFactory;
	
	/** @var boolean */
	private $googleConnected;
	
	/** @var boolean */
	private $facebookConnected;
	
    /** @var boolean */
	private $sklikConnected;
    
	/** @var string */
	private $facebookAccessToken;
	
	/** @var string */
	private $googleAccessToken;
	
	/** @var string */
	private $googleRefreshToken;
    
    /** @var SklikAdAccounts */
	private $sklikAuthorization;
	
	private $googleAuth;
	
	public function __construct(
		App\Model\UserManager $usersManager,
		SignInFormFactorySklik $signInFactorySklik,
        SklikAdAccounts $sklikAuthorization,
		GoogleAuthorisation $googleAuth) 
	{
		parent::__construct($usersManager);
		
		$this->sklikFormFactory = $signInFactorySklik;
        $this->sklikAuthorization = $sklikAuthorization;
		$this->googleAuth = $googleAuth;
	}
	
	// Overeni autorizace, kod se provede pri startu presenteru.
	// Pokud byl jiz uzivatel autorizoval pristup k facebooku, google, seznam
	// provede se autentizace.	
	protected function startup() {
		parent::startup();
		
		$userId = $this->getUser()->getId();
		$accessToken = $this->usersManager->getGoogleAccessToken($userId);
		
		$isConnectionValid = $this->checkIfGoogleConnectionValid($accessToken);
		
		$this->googleConnected = $isConnectionValid;
		
		if (!$isConnectionValid && $accessToken) {
				$accessToken = $this->googleAuth->renewAccessToken($userId);
				$this->googleConnected = $this->checkIfGoogleConnectionValid($accessToken);
		}
		
        /*
         * Sklik autentizace
         */
        $renewed = $this->usersManager->renewUsersActivatedSklikSession($userId);
        if ($renewed) {
            $this->sklikConnected = true;
        } else {
			$this->sklikDisconnect();
        }
	}
	
	private function checkIfGoogleConnectionValid($accessToken) {
		$valid = false;
		
		if ($accessToken) {
			$isValid = $this->googleAuth->isAccessTokenStillValid($accessToken);
			if ($isValid) {
				$valid = true;
			}
		}
		return $valid;
	}
	
	public function renderDefault()
	{
		$this->facebookConnected = false;

		$this->template->facebookConnected = $this->facebookConnected;
		$this->template->googleConnected = $this->googleConnected;
        $this->template->sklikConnected = $this->sklikConnected;
	}
	
	/**
	 * 
	 * Pokud facebook účet pro přihlášeného uživatele již neexistuje, vloží jej
	 * do databáze.
	 */
	protected function registerFbAccountForUser($fbAccountId, $me) {
		$userId = $this->getUser()->getId();
		$doesExists = $this->usersManager->DoesFbAccountExitsForUser($fbAccountId, $userId);
		if (!$doesExists) {
			/**
			 * Variable $me contains all the public information about the user
			 * including facebook id, name and email, if he allowed you to see it.
			 */
			$existing = $this->usersManager->RegisterFromFacebook($fbAccountId, $me, $userId);
		}
	}
	
	/**
	 * Zjistí zda je facebook klient úspěšně připojen. Pokud není
	 * vyhodí chybovou hlášku.
	 * 
	 * @param \Facebook\Facebook $fbClient
	 */
	protected function checkFbConnection($fbClient) {
		if (!$fbClient->getUser()) {
			$this->flashMessage($this->translator->translate('messages.services.facebook_connect_error'));
		} 
	}
	
	/**
	 * Aktivuje facebook účet přihlášeného uživatele.
	 */
	protected function activateUsersFbAccount($fbAccountId) {
		$userId = $this->getUser()->getId();
		$this->usersManager->activateUsersFacebookAccount($userId, $fbAccountId);
	}
	
	/**
	 * Aktualizuje přístupový token facebook účtu přihlášeného uživatele.
	 */
	protected function updateUsersFbAcessToken($fbId, $accessToken) {
		$this->usersManager->UpdateFacebookAccessToken($fbId, $accessToken);
	}
	
	/**
	 * Aktivuje facebook účet přihlášeného uživatele.
	 */
	protected function activateUsersGoogleAccount($googleAccountId) {
		$userId = $this->getUser()->getId();
		$this->usersManager->activateUsersGoogleAccount($userId, $googleAccountId);
	}
	
	/** 
	 * Vytvořím si komponentu pro připojení 
	 * facebook účtu k přihlášenému živateli
	 * 
	 * @return \Kdyby\Facebook\Dialog\LoginDialog 
	 */
	protected function createComponentFbLogin() {
		/** @var \Kdyby\Facebook\Dialog\LoginDialog $dialog */
		$dialog = $this->facebook->createDialog('login');
		
		$dialog->onResponse[] = function (\Kdyby\Facebook\Dialog\LoginDialog $dialog) {
			/** @var \Facebook\Facebook $fb */
			$fb = $dialog->getFacebook();
			$this->checkFbConnection($fb);
			
			try {
				// ziskám facebook profil
				$me = $fb->api('/me', NULL, ['fields' => 'email, id, about, name']);
				
				$fbId = $fb->getUser();
				$userId = $this->getUser()->getId();
				
				$this->registerFbAccountForUser($fbId, $me);
				$this->activateUsersFbAccount($fbId, $userId);
				$fb->setExtendedAccessToken();
				$accessToken = $fb->getAccessToken();
				$this->updateUsersFbAcessToken($fbId, $accessToken);
			} catch (\Kdyby\Facebook\FacebookApiException $e) {
				\Tracy\Debugger::exceptionHandler($e);
			} 
			$this->redirect('this');
		};
		return $dialog;
	}

	/** @return \Kdyby\Google\Dialog\LoginDialog */
	protected function createComponentConnectGoogle()
	{
		$component = new connectGoogle($this->googleAuth);
		return $component;
	}
	
	protected function createComponentSklikLoginForm() {
		$form = $this->sklikFormFactory->create(function () {
            $this->redirect('this');
		});
        return $form;
	}

	protected function createComponentFbDisconnect()
	{
		$form = new Form();
		$form->addSubmit('facebookDisconnect', 'Odpojit');
		$form->onSubmit[] = [$this, 'fbDisconnect'];
		$renderer = $form->getRenderer();
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';
		return $form;
	}
	
	protected function createComponentGoogleDisconnect()
	{
		$form = new Form();
		$form->addSubmit('googleDisconnect', 'Odpojit');
		$form->onSubmit[] = [$this, 'googleDisconnect'];
		$renderer = $form->getRenderer();
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';
		return $form;
	}
    
    protected function createComponentSklikDisconnect()
	{
		$form = new Form();
		$form->addSubmit('sklikDisconnect', 'Odpojit');
		$form->onSubmit[] = [$this, 'sklikDisconnect'];
		$renderer = $form->getRenderer();
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';
		return $form;
	}
	
	public function fbDisconnect() {
		$userId = $this->getUser()->getId();
		$fbAccountId = $this->facebook->getUser();
		$this->usersManager->deactivateUsersFacebookAccount($userId, $fbAccountId);
		$this->facebook->destroySession();

	}
	
	public function googleDisconnect() {
		$userId = $this->getUser()->getId();
		$accessToken = $this->usersManager->getGoogleAccessToken($userId);
		if ($accessToken) {
			$googleAccountId = $this->googleAuth->getUserId($accessToken);
			$this->usersManager->deactivateUsersGoogleAccount($userId, $googleAccountId);
			$this->redirect('this');
		}
	}
    
    public function sklikDisconnect() {
		$userId = $this->getUser()->getId();
        $sklikAccountPk = $this->usersManager->getUsersActivatedSklikAccountPk($userId);
		if ($sklikAccountPk) {
			$this->usersManager->deactivateUsersSklikAccount($userId, $sklikAccountPk);
		}
        
		$this->sklikConnected = false;
	}
    
}
