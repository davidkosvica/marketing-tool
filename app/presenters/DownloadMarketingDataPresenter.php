<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;
use App;
use App\Model\SklikManager;
use App\Model\Google\GoogleAuthorisation;
use App\Model\AdwordsManager;
use App\Model\FacebookBusinessManager;
use App\Model\UserManager;
use App\Model\Analytics\AnalyticsStats;
//use App\Model\Adwords\AdwordsCustomers;

/**
 * Description of ManageServicesPresenter
 *
 * @author davidkosvica
 */
class DownloadMarketingDataPresenter extends UserAreaPresenter {
	
	/** @var App\Model\AdwordsManager $adwords */
	private $adwords;
	
	/** @var App\Model\FacebookBusinessManager $facebook */
	private $facebook;
	
	/** @var App\Model\SklikManager $sKlik */
	private $sKlik;
	
	private $analyticsStats;
	
	private $adwordsCustomers;
	
	private $googleAuth;
	
	function __construct(AdwordsManager $adwords,
						 FacebookBusinessManager $facebook,
						 UserManager $usersManager,
                         SklikManager $sKlik,
						 AnalyticsStats $analyticsStats,
						 GoogleAuthorisation $googleAuth) {
		parent::__construct($usersManager);
		
		$this->adwords = $adwords;
		$this->facebook = $facebook;
		$this->sKlik = $sKlik;
		$this->googleAuth = $googleAuth;
		$this->analyticsStats = $analyticsStats;
	}
	
	
	/*
	 * Metoda slouzi k debugovani, vytiskne mi potrebnou vyjimku.
	 * Vhodne v situacich, kdy nechci ukoncit na zaklade jedne vyjimky cely
	 * kod, ale chci aby se vykonal i zbytek.
	 */
	protected function printError($e, $whichService, $usrId) {
		$errorMsg = $e->getMessage();
		\Tracy\Debugger::$maxLength = 3000;
		\Tracy\Debugger::$maxDepth = 300;
		\Tracy\Debugger::dump("User ID: {$usrId}, " . $whichService . " error:");
		\Tracy\Debugger::dump($errorMsg);
		\Tracy\Debugger::barDump($e);
	}
	
	/*
	 * Metoda stahne data o Adwords kampanich
	 */
	protected function downloadUsersAdwordsData($refreshToken, $userId) {
		if (!$refreshToken) {
			throw new \Exception("Adwords: Uzivatel nema v DB refresh token.");
		}
		$this->adwords->downloadAdwordsData($userId, $refreshToken);
		$this->adwords->downloadPerformanceStatistics($refreshToken);
		\Tracy\Debugger::dump("User ID: {$userId}, Adwords finished successfully");
	}
	
	/*
	 * Metoda stahne data o FacebookAds kampanich
	 */
	protected function downloadUsersFacebookAdsData($accessToken, $userId) {
		if (!$accessToken) {
			throw new \Exception("Facebook Ads: Uzivatel nema v DB access token.");
		}
		$this->facebook->downloadFacebookBusinessData($accessToken, $userId);
		\Tracy\Debugger::dump("User ID: {$userId}, Facebook Ads finished successfully");
	}
    
   
	protected function downloadUsersSklikAdsData($userId) {
		$this->sKlik->downloadSklikData($userId);
		\Tracy\Debugger::dump("User ID: {$userId}, Sklik finished successfully");
	}

	protected function startup() {
		parent::startup();
		
		$userIds = $this->usersManager->getUsersIds();
		foreach($userIds as $userId) {
            $accessToken = $this->usersManager->getGoogleAccessToken($userId);
			
			if ($accessToken) {
				if (!$this->googleAuth->isAccessTokenStillValid($accessToken)) {
					$accessToken = $this->googleAuth->renewAccessToken($userId);
				}
				$analytics = $this->analyticsStats->createAnalyticsObject($accessToken);
//				$this->analyticsStats->getReport($analytics, '92320289');
				$this->analyticsStats->getReport($analytics, '92324711');

			}
			
//			$refreshToken = $this->usersManager->getGoogleRefreshToken($userId);
//			$this->downloadUsersAdwordsData($refreshToken, $userId);
			
//			$accessToken = $this->usersManager->getFacebookAccessToken($userId);
//			$this->downloadUsersFacebookAdsData($accessToken, $userId);
//
//			$this->downloadUsersSklikAdsData($userId);
 		}
	} 
}
