<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Components\ConnectingServices;
use Nette\Application\UI;
use App\Model\Google\GoogleAuthorisation;
use Google_Service_Analytics;


/**
 * Description of connectGoogle
 *
 * @author david
 */
class connectGoogle extends UI\Control {
	private $googleAuth;
	public function __construct(GoogleAuthorisation $googleAuth)
	{
		parent::__construct();
		$this->googleAuth = $googleAuth;
	}
	
	public function handleConnect()
	{
		$this->googleAuth->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
		$this->googleAuth->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/google-oauth2-callback');
		$this->googleAuth->redirectToAuthPage();
	}

}
