<?php
require __DIR__ . '/../vendor/autoload.php';


$configurator = new Nette\Configurator;

$configurator->setDebugMode('176.74.148.24'); // enable for your remote IP (netwings)
//$configurator->setDebugMode('83.167.225.161'); // enable for your remote IP (vlak cd)
//$configurator->setDebugMode('178.77.242.211'); // enable for your remote IP (doma)
//$configurator->setDebugMode('109.81.182.28'); // dobrovskeho kavarna
//$configurator->setDebugMode('195.178.73.231'); // koleje

$configurator->enableTracy(__DIR__ . '/../../../../nette_log');
error_reporting(~E_USER_DEPRECATED);

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../../../../nette_temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
