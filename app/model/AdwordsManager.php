<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Nette;
use App\Model\Adwords\AdwordsAdGroups;
use App\Model\Adwords\AdwordsKeywords;
use App\Model\Adwords\AdwordsCampaigns;
use App\Model\Adwords\AdwordsCustomers;
use App\Model\Adwords\DownloadCriteriaReportWithSelector;
use App\Model\Adwords\GetReportFields;
use App\Model\DbInterface\AdWords\AdwordsAccountTable;
//use App\Model\Adwords\ParallelReportDownload;
//use Google\AdsApi\AdWords\v201708\cm\ReportDefinitionReportType;
//use Google\AdsApi\AdWords\v201802\cm\ReportDefinitionReportType;

use Nette\Database\Context;

/**
 * Description of AdwordManager
 *
 * @author davidkosvica
 */
class AdwordsManager {
    use Nette\SmartObject;
	
	private $adwordsKeywords;

	/**
     * @var \App\Model\Adwords\AdGroups $adwordsAdGroups
     */
    private $adwordsAdGroups;

    /**
     * @var \App\Model\Adwords\AdwordsCustomers $adwordsCustomers
     */
    private $adwordsCustomers;

    /**
     * @var \App\Model\Adwords\AdwordsCampaigns $adwordsCampaigns
     */
    private $adwordsCampaigns;

    /**
     * @var \App\Model\Adwords\DownloadCriteriaReportWithSelector $downloadCriteriaReport
     */
    private $downloadCriteriaReport;

    /**
     * @var \App\Model\Adwords\GetReportFields $getReportFields
     */
    private $getReportFields;

    /**
     * @var \App\Model\Adwords\ParallelReportDownload $parallelReportDownload
     */
    private $parallelReportDownload;

    /**
     * @var \App\Model\UserManager $usersManager
     */
    private $usersManager;

    /**
     * @var \Nette\Database\Context $database
     */
    private $database;
	
    public function __construct(Context $database) {
		$this->database = $database;
    }
	
    /**
     * @param string $refreshToken
     * @param integer $idUser
     */
    public function downloadAdwordsData($idUser, $refreshToken) {
		$this->adwordsCustomers = new AdwordsCustomers($this->database, $refreshToken);
		$this->adwordsCampaigns = new AdwordsCampaigns($this->database, $refreshToken);
		$this->adwordsAdGroups = new AdwordsAdGroups($this->database, $refreshToken);
		$this->adwordsKeywords = new AdwordsKeywords($this->database, $refreshToken);
		$this->getReportFields = new GetReportFields($refreshToken);

		
		$directlyAccessibleCustomers = $this->adwordsCustomers->getDirectlyAccessibleCustomers();
		
		foreach ($directlyAccessibleCustomers as $customer) {
				$this->adwordsCustomers->putAdwordCustomersToDb($idUser, $customer);
		}
		
		$rootAccountId = $directlyAccessibleCustomers[0]->getCustomerId();
		$this->adwordsCustomers->buildSession($rootAccountId);
		
		$customers = $this->adwordsCustomers->getCustomers();

		$this->adwordsCampaigns->setCampaignsForCustomers($customers);

		$campaignsOfCustomers = $this->adwordsCampaigns->getCampaignsOfManagedCustomers($customers);

		foreach ($campaignsOfCustomers as $customerId => $campaigns) {
				$this->adwordsAdGroups->setAdGroupsForCampaigns($campaigns, $customerId);
		}
		$idsActiveRows = $this->adwordsCustomers->getAdwordsCustomersIdsFromDb();
		foreach ($idsActiveRows as $idActiveRow) {
			if (!$idActiveRow->{AdwordsAccountTable::IS_MANAGER_ACCOUNT}) {
				/* @var $idActiveRow \Nette\Database\Table\ActiveRow */
				$id = $idActiveRow->{AdwordsAccountTable::CUSTOMER_ID};
				$this->adwordsKeywords->buildSession($id);
				$keywordsReport = $this->adwordsKeywords->getKeywords();
				$keywordsArray = $this->csvToArray($keywordsReport);
				$this->adwordsKeywords->putKeywordsInDb($keywordsArray);
			}
		}
    }

    public function downloadPerformanceStatistics($refreshToken) {
		$this->adwordsCustomers = new AdwordsCustomers($this->database, $refreshToken);
		$this->getReportFields = new GetReportFields($refreshToken);
		
		$this->downloadCriteriaReport = new DownloadCriteriaReportWithSelector($refreshToken,
			$this->database);
		
		$idsActiveRows = $this->adwordsCustomers->getAdwordsCustomersIdsFromDb();
		foreach ($idsActiveRows as $idActiveRow) {
			if (!$idActiveRow->{AdwordsAccountTable::IS_MANAGER_ACCOUNT}) {
				/* @var $idActiveRow \Nette\Database\Table\ActiveRow */
				$id = $idActiveRow->{AdwordsAccountTable::CUSTOMER_ID};
				$this->downloadCriteriaReport->buildSession($id);
				
				// downloading performance statistics
				$campaignReport = $this->downloadCriteriaReport->downloadCampaignReport();
				$campaignReportValues = $this->csvToArray($campaignReport);
				$this->downloadCriteriaReport->putCampaignReportInDb($campaignReportValues);

				$adGroupReport = $this->downloadCriteriaReport->downloadAdGroupReport();
				$adGroupReportValues = $this->csvToArray($adGroupReport);
				$this->downloadCriteriaReport->putAdGroupReportInDb($adGroupReportValues);
				
				$keywordsReport = $this->downloadCriteriaReport->downloadKeywordReport();
				$keywordReportValues = $this->csvToArray($keywordsReport);
				$this->downloadCriteriaReport->putKeywordReportInDb($keywordReportValues);
			}
		}
    }
	
	private function csvToArray($csvString) {
		$csv = array_map('str_getcsv', explode("\n", $csvString));
		array_pop($csv); # delete last line from $csvString, it contains null
		array_walk($csv, function(&$a) use ($csv) {
		  $a = array_combine($csv[0], $a);
		});
		array_shift($csv); # remove column header
		
		return $csv;
	}


	public function getAvailableColumns($reportType) {
		$fieldsString = $this->getReportFields->get($reportType);
		\Tracy\Debugger::$maxLength = 100000;
		\Tracy\Debugger::barDump($fieldsString);
	}
	
}