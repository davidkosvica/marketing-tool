<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use Nette;
use FacebookAds\Object\AdAccount;
use App\Model\FacebookBusiness\FacebookBusinessCampaigns;
use App\Model\FacebookBusiness\FacebookBusinessAdSets;
use App\Model\FacebookBusiness\FacebookBusinessAds;
use App\Model\FacebookBusiness\FacebookBusinessAdAccounts;
use App\Model\FacebookBusiness\FacebookBusinessAuthorization;
use App\Model\FacebookBusiness\FacebookBusinessCampaignStats;
use App\Model\FacebookBusiness\FacebookBusinessAdSetsStats;
use App\Model\FacebookBusiness\FacebookBusinessAdsStats;

/**
 * Description of FacebookBusinessManager
 *
 * @author davidkosvica
 * 
 */
class FacebookBusinessManager {
	
	/**
	 * @var \App\Model\FacebookBusiness\FacebookBusinessCampaigns $fbBusinessCampaigns
	 */
	private $fbBusinessCampaigns;
	
	/**
	 * @var \FacebookBusiness\FacebookBusinessAuthorization $fbBusinessAuthorization
	 */
	private $fbBusinessAuthorization;
	
	/**
	 * @var \App\Model\FacebookBusiness\FacebookBusinessAdAccounts $fbBusinessAdAccounts
	 */
	private $fbBusinessAdAccounts;
	
	/**
	 * @var \App\Model\FacebookBusiness\FacebookBusinessAdSets $fbBusinessAdSets
	 */
	private $fbBusinessAdSets;
	
	/**
	 * @var \App\Model\FacebookBusiness\FacebookBusinessAds $fbBusinessAds
	 */
	private $fbBusinessAds;
	
	private $facebookBusinessCampaignStats;
	private $facebookBusinessAdSetsStats;
	private $facebookBusinessAdsStats;
	
	/**
	 * @param Nette\Database\Context $database
	 */

	public function __construct(FacebookBusinessCampaignStats $facebookBusinessCampaignStats,
		FacebookBusinessAdSetsStats $facebookBusinessAdSetsStats,
		FacebookBusinessAdsStats $facebookBusinessAdsStats,
		FacebookBusinessAds $facebookBusinessAds,
		FacebookBusinessAdSets $facebookBusinessAdSets,
		FacebookBusinessCampaigns $facebookBusinessCampaigns,
		FacebookBusinessAdAccounts $facebookBusinessAdAccounts)
	{
		$this->facebookBusinessCampaignStats = $facebookBusinessCampaignStats;
		$this->facebookBusinessAdsStats = $facebookBusinessAdSetsStats;
		$this->facebookBusinessAdSetsStats = $facebookBusinessAdsStats;
		$this->fbBusinessAdAccounts = $facebookBusinessAdAccounts;
		$this->fbBusinessAdSets = $facebookBusinessAdSets;
		$this->fbBusinessAds = $facebookBusinessAds;
		$this->fbBusinessCampaigns = $facebookBusinessCampaigns;
	}
	
	public function downloadFacebookBusinessData($accessToken, $idUser) {
		
		$this->fbBusinessAuthorization = new FacebookBusinessAuthorization($accessToken);
		$this->fbBusinessAdAccounts->setAdAccountsOfLoggedFbUser($accessToken);
		
		$adAccountIds = $this->fbBusinessAdAccounts->getAdAccountsIdsFromDb($accessToken);
		foreach ($adAccountIds as $adAccountId) {
			$adAccount = new AdAccount('act_' . $adAccountId);
			$campaigns = $this->fbBusinessCampaigns->setCampaignsOfAdAccount($adAccount);
			foreach ($campaigns as $campaign) {
				$campaignStatistics = $this->facebookBusinessCampaignStats->getStatistics($campaign);
				$adSets = $this->fbBusinessAdSets->setAdSets($campaign);
				foreach ($adSets as $adSet) {
					$adSetStatistics = $this->facebookBusinessAdSetsStats->getStatistics($adSet);
					$ads = $this->fbBusinessAds->setAdsOfAdSet($adSet);
					foreach ($ads as $ad) {
						$this->facebookBusinessAdsStats->getStatistics($ad);
					}
				}
			}
		}
	}
	
}

class AccessTokenFbIsNull extends \Exception {
	public function __construct() {
        
		$message = 'Access token for fb must be set to '
				. 'access ad accounts';
		
        parent::__construct($message);
    }
}