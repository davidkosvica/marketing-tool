<?php 

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Analytics;
use App\Model\Google\GoogleAuthorisation;
use App\Model\UserManager;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsRequest;


/**
 * Description of AnalyticsStats
 *
 * @author david
 */
class AnalyticsStats {
	
	private $googleAuth;
	private $userManager;
	
	public function __construct(GoogleAuthorisation $googleAuth,
		UserManager $userManager) 
	{
		$this->googleAuth = $googleAuth;
		$this->userManager = $userManager;
	}
	
	public function setAccessToken($accessToken) {
		$this->googleAuth->setAccessToken($accessToken);
	}
	
	public function createAnalyticsObject($accessToken) {
		$this->setAccessToken($accessToken);
		$analytics = new Google_Service_AnalyticsReporting($this->googleAuth->getClient());
		return $analytics;
	}
	
	public function getReport($analytics, $viewId) {
		// Create the DateRange object.
		$dateRange = new Google_Service_AnalyticsReporting_DateRange();
		$dateRange->setStartDate("7daysAgo");
		$dateRange->setEndDate("today");

		// Create the Metrics object.
		$sessions = new Google_Service_AnalyticsReporting_Metric();
		$sessions->setExpression("ga:sessions");
		$sessions->setAlias("sessions");

		// Create the ReportRequest object.
		$request = new Google_Service_AnalyticsReporting_ReportRequest();
		$request->setViewId($viewId);
		$request->setDateRanges($dateRange);
		$request->setMetrics(array($sessions));

		$body = new Google_Service_AnalyticsReporting_GetReportsRequest();
		$body->setReportRequests( array( $request) );
		return $analytics->reports->batchGet( $body );
	}
	
	public function printResults($reports) {
		for($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
			$report = $reports[$reportIndex];
			
		}
	}	
}
