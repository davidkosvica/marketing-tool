<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use Nette;
use FacebookAds\Object\Fields\AdsInsightsFields;

/**
 * Description of FacebookBusinessDownloadStats
 *
 * @author david
 */
class FacebookBusinessDownloadStats {
	public function getStatistics($adObject, $fields = null, $params = null) {
		/* @var $campaign \FacebookAds\Object\Campaign */
		if ($params == null) {
			$params = array(
				'time_range' => array(
					'since' => (new \DateTime("-1 week"))->format('Y-m-d'),
					'until' => (new \DateTime())->format('Y-m-d'),
				),
			);
		}
		
		if ($fields == null) {
			$fields = array(
				AdsInsightsFields::IMPRESSIONS,
				AdsInsightsFields::UNIQUE_CLICKS,
				AdsInsightsFields::ACTIONS,
				AdsInsightsFields::CPC,
				AdsInsightsFields::CTR,
			);
		}
		
		
		$insight = $adObject->getInsights($fields, $params);
		
		/* @var $output \FacebookAds\Cursor */
		
		return $insight;
	}
}
