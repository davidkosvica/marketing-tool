<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use Nette;
use FacebookAds\Api;

/**
 * Description of FacebookBusinessAuthorization
 *
 * @author davidkosvica
 */
class FacebookBusinessAuthorization {
	use Nette\SmartObject;
	
	/** @var string Pristupovy token k autentizaci/autorizaci uzivatele  */
	protected $accessToken;
	
	const APP_ID = '164513844097447';
	const APP_SECRET = '6a72976a5bf071a5377a55d3a00436ca';

	public function __construct($accessToken) {
		$this->setAccessToken($accessToken);
	}
	
	private function setAccessToken($accessToken) {
		$this->accessToken = $accessToken;
		Api::init(self::APP_ID, self::APP_SECRET, $this->accessToken);
	}
}
