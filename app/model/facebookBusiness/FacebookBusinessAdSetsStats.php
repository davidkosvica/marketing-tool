<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use Nette;
use App\Model\DbInterface\FacebookBusiness\FacebookAdSetStatsTable;
use App\Model\FacebookBusiness\FacebookBusinessDownloadStats;
/**
 * Description of FacebookBusinessAdSetsStats
 *
 * @author david
 */
class FacebookBusinessAdSetsStats {
	
	private $adSetsStatsTable;
	private $downloadStats;
	
	public function __construct(FacebookAdSetStatsTable $adSetsStatsTable,
		FacebookBusinessDownloadStats $downloadStats) {
		$this->adSetsStatsTable = $adSetsStatsTable;
		$this->downloadStats = $downloadStats;
	}
	
	public function getStatistics($adSet) {
		
		$stats = $this->downloadStats->getStatistics($adSet);
		
	}
	
	public function putInDb() {
		
	}
}
