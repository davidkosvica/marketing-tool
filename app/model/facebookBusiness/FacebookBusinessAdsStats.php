<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;

use Nette;

use App\Model\FacebookBusiness\FacebookBusinessDownloadStats;
use App\Model\DbInterface\FacebookBusiness\FacebookAdStatsTable;

/**
 * Description of FacebookBusinessAdsStats
 *
 * @author david
 */
class FacebookBusinessAdsStats {
	
	private $adsStatsTable;
	private $downloadStats;

	public function __construct(FacebookAdStatsTable $adsStatsTable,
		FacebookBusinessDownloadStats $downloadStats) {
		$this->adsStatsTable = $adsStatsTable;
		$this->downloadStats = $downloadStats;
	}
	
	public function getStatistics($ad) {
		$stats = $this->downloadStats->getStatistics($ad);
		
	}
	
	public function putInDb() {
		
	}
}
