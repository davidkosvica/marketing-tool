<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;

use Nette;
use App\Model\FacebookBusiness\FacebookBusinessDownloadStats;
use App\Model\DbInterface\FacebookBusiness\FaceboookCampaignStatsTable;

/**
 * Description of FacebookBusinessCampaignStats
 *
 * @author david
 */
class FacebookBusinessCampaignStats {
	
	private $database;
	private $downloadStats;
	private $campaignStatsTable;

	public function __construct(FaceboookCampaignStatsTable $campaignStatsTable, 
		FacebookBusinessDownloadStats $downloadStats) {
		$this->campaignStatsTable = $campaignStatsTable;
		$this->downloadStats = $downloadStats;
	}
	
	public function getStatistics($campaign) {
		
		$stats = $this->downloadStats->getStatistics($campaign);
		
	}
	
	public function putInDb() {
		
	}
}
