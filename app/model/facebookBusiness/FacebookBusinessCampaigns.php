<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Values\ArchivableCrudObjectEffectiveStatuses;
use FacebookAds\Object\Fields\CampaignFields;
use App\Model\DbInterface\FacebookBusiness\FacebookCampaignTable;
use Nette;
use FacebookAds\Object\Fields\AdsInsightsFields;


/**
 * Description of FacebookBusinessCampaigns
 *
 * @author davidkosvica
 */
class FacebookBusinessCampaigns {
	
	/**
	 * @param Nette\Database\Context $database
	 */
	private $database;
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FacebookCampaignTable
	 */
	private $facebookCampaignTable;
	
	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
		$this->facebookCampaignTable = new FacebookCampaignTable($database);
	}
	
	/**
	 * @param longint $adAccountId
	 * @return \FacebookAds\Cursor
	 */
	private function getCampaignsOfAdAccount($adAccount) {
		$campaigns = $adAccount->getCampaigns(
			array(CampaignFields::ACCOUNT_ID, 
				CampaignFields::ADLABELS, 
				CampaignFields::BOOSTED_OBJECT_ID,
				CampaignFields::BRAND_LIFT_STUDIES,
				CampaignFields::BUDGET_REBALANCE_FLAG,
				CampaignFields::BUYING_TYPE,
				CampaignFields::CAN_CREATE_BRAND_LIFT_STUDY,
				CampaignFields::CAN_USE_SPEND_CAP,
				CampaignFields::CONFIGURED_STATUS,
				CampaignFields::CREATED_TIME,
				CampaignFields::EFFECTIVE_STATUS,
				CampaignFields::ID,
				CampaignFields::NAME,
				CampaignFields::OBJECTIVE,
				CampaignFields::RECOMMENDATIONS,
				CampaignFields::SOURCE_CAMPAIGN,
				CampaignFields::SOURCE_CAMPAIGN_ID,
				CampaignFields::SPEND_CAP,
				CampaignFields::START_TIME,
				CampaignFields::STATUS,
				CampaignFields::STOP_TIME,
				CampaignFields::UPDATED_TIME,
				CampaignFields::ADBATCH,
				CampaignFields::EXECUTION_OPTIONS,
				CampaignFields::ITERATIVE_SPLIT_TEST_CONFIGS,
				CampaignFields::PROMOTED_OBJECT,
			), 
			array(CampaignFields::EFFECTIVE_STATUS => array(
					ArchivableCrudObjectEffectiveStatuses::ACTIVE,
					ArchivableCrudObjectEffectiveStatuses::PAUSED,
				)
			)
		);	
		
		return $campaigns;
	}
	
	/**
	 * 
	 * @param \Nette\Database\Table\ActiveRow $adAccount
	 */
	public function setCampaignsOfAdAccount($adAccount) {
		$iterableListOfCampaigns = $this->getCampaignsOfAdAccount($adAccount);
		$campaigns = $iterableListOfCampaigns->getArrayCopy();
		foreach ($campaigns as $campaign) {
			$id = $adAccount->getData()['id'];
			$this->putCampaignToDb($campaign, substr($id, 4));
		}
		
		return $campaigns;
	}
	
	/**
	 * 
	 * @param Campaign $campaign
	 * @param longint $adAccount
	 */
	private function putCampaignToDb(\FacebookAds\Object\Campaign $campaign, $adAccount) {
		$campaignData = $campaign->getData();
		try {
			$this->facebookCampaignTable->insertCampaign($campaignData, $adAccount);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {}
	}
}
