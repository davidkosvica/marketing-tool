<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use FacebookAds\Object\Values\ArchivableCrudObjectEffectiveStatuses;
use FacebookAds\Object\Fields\AdFields;
use App\Model\DbInterface\FacebookBusiness\FacebookAdTable;
use Nette;

/**
 * Description of FacebookBusinessAds
 *
 * @author davidkosvica
 */
class FacebookBusinessAds {
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FacebookAdTable
	 */
	private $facebookAdTable;
	
	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
		$this->facebookAdTable = new FacebookAdTable($database);
	}
	
	/**
	 * 
	 * @param \FacebookAds\Object\AdSet $adSet
	 */
	public function setAdsOfAdSet($adSet) {
		$ads = $adSet->getAds(array(
			AdFields::NAME,
			AdFields::CONFIGURED_STATUS,
			AdFields::EFFECTIVE_STATUS,
			AdFields::CREATIVE,
			AdFields::ADSET_ID,
		), array(
			AdFields::EFFECTIVE_STATUS => array(
			  ArchivableCrudObjectEffectiveStatuses::ACTIVE,
			  ArchivableCrudObjectEffectiveStatuses::PAUSED,
			  ArchivableCrudObjectEffectiveStatuses::PENDING_REVIEW,
			  ArchivableCrudObjectEffectiveStatuses::PREAPPROVED,
			),
		));

		$idAdSet = $adSet->getData()['id'];
		$adsCursor = $ads;

		$ads = $ads->getArrayCopy();

		foreach ($ads as $ad) {
			$this->putAdToDb($ad);
		}

		return $adsCursor;
	}
	
	/** 
	 * @param \FacebookAds\Object\Ad $ad 
	 */
	private function putAdToDb($ad) {
		$adData = $ad->getData();
		try {
			$this->facebookAdTable->insertAd($adData);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			$this->facebookAdTable->updateAd($adData);
		}
	}
}
