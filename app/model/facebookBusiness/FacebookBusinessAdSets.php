<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use FacebookAds\Object\Fields\AdSetFields;
use App\Model\DbInterface\FacebookBusiness\FacebookAdSetTable;
use Nette;

/**
 * Description of FacebookBusinessAdSets
 *
 * @author davidkosvica
 */
class FacebookBusinessAdSets {
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FacebookAdSetTable
	 */
	private $facebookAdSetTable;
	
	public function __construct(Nette\Database\Context $database) {
		$this->facebookAdSetTable = new FacebookAdSetTable($database);
	}
	
	/**
	 * 
	 * @param \FacebookAds\Object\Campaign $campaign
	 * @return \FacebookAds\Object\AdSet
	 */
	public function setAdSets($campaign) {
		$adSets = $campaign->getAdSets(array(
		  AdSetFields::NAME,
		  AdSetFields::START_TIME,
		  AdSetFields::END_TIME,
		  AdSetFields::DAILY_BUDGET,
		  AdSetFields::LIFETIME_BUDGET,
		  AdSetFields::ID,
		  AdSetFields::STATUS,
		  AdSetFields::CREATIVE_SEQUENCE,
		  AdSetFields::CAMPAIGN_ID
		));
		
		$idCampaign = $campaign->getData()['id'];
		
		$adSets = $adSets->getArrayCopy();
		
		foreach ($adSets as $adSet) {
			$this->putAdSetToDb($adSet);
		}
		return $adSets;
	}
	
	/**
	 * 
	 * @param \FacebookAds\Object\AdSet $adSet
	 */
	private function putAdSetToDb($adSet) {
		$adSetData = $adSet->getData();
		
		try {
			$this->facebookAdSetTable->insertAdSet($adSetData);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			$this->facebookAdSetTable->updateAdSet($adSetData);
		}
	}
}
