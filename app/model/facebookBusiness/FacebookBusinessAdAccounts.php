<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\FacebookBusiness;
use Nette;
use FacebookAds\Object\AdAccountUser;
use App\Model\DbInterface\Facebook\FacebookAccountTable;
use App\Model\DbInterface\FacebookBusiness\FacebookAdAccountTable;
use App\Model\DbInterface\FacebookBusiness\FbAccountToFbAdAccountTable;


/**
 * Description of FacebookBusinessAdAccounts
 *
 * @author davidkosvica
 */
class FacebookBusinessAdAccounts {
	
	/**
	 * @var App\Model\DbInterface\Facebook\FacebookAccountTable
	 */
	private $facebookAccountTable;
	
	/**
	 * @var App\Model\DbInterface\Facebook\UserToFbAccount
	 */
	private $userToFbAccount;
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FacebookAdAccountTable
	 */
	private $facebookAdAccountTable;
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FbAccountToFbAdAccountTable
	 */
	private $fbAccountToFbAdAccountTable;
	
	public function __construct(Nette\Database\Context $database) {
		$this->facebookAccountTable = new FacebookAccountTable($database);
		$this->facebookAdAccountTable = new FacebookAdAccountTable($database);
		$this->fbAccountToFbAdAccountTable = new FbAccountToFbAdAccountTable($database);
	}
	
	/** 
	 * Prida soucasny Ad Account ucet do databaze 
	 * (tabulka "facebook_ad_account", ke kteremu ma profil
	 * s danym access tokenem pristup.
	 */
	public function setAdAccountsOfLoggedFbUser($accessToken) {
		$me = new AdAccountUser();
		$me->setId('me');
		$adAccountsArray = $me->getAdAccounts()->getArrayCopy();
		
		$accountPrimaryKey = $this->facebookAccountTable->getAccountPkBaseOnAccessToken($accessToken);
		foreach ($adAccountsArray as $adAccount) {
			$this->putAdAccountToDB($adAccount, $accountPrimaryKey);
		}
	}
	
	/** 
	 * Vlozi Ad Account do tabulky "facebook_ad_account"
	 * @param \FacebookAds\Object\AdAccount $adAccount
	 * @param int $accountPk
	 */
	private function putAdAccountToDB($adAccount, $accountPk) {
		$accountData = $adAccount->getData();
		$adAccountId = $accountData['account_id'];
			
		try {
			$this->facebookAdAccountTable->insertAdAccount($adAccountId, $accountPk);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	public function getAdAccountsIdsFromDb($accessToken) {
		$fbAccountPk = $this->facebookAccountTable->getAccountPkBaseOnAccessToken($accessToken);
		$primaryKeys = $this->fbAccountToFbAdAccountTable->getPrimaryKeysOfAdAccountsOfFbAccount($fbAccountPk);
        $facebookIDs = [];
        foreach ($primaryKeys as $primaryKey) {
            $fbId = $this->facebookAdAccountTable->getAccountBaseOnPk($primaryKey);
            $facebookIDs[] = $fbId;
        }
        return $facebookIDs;
	}
}
