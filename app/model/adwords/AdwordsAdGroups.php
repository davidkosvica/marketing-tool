<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;
use Nette;
use App\Model\Adwords\AdwordsAuthorization;
use Google\AdsApi\AdWords\v201802\cm\OrderBy;
use Google\AdsApi\AdWords\v201802\cm\Paging;
use Google\AdsApi\AdWords\v201802\cm\Selector;
use Google\AdsApi\AdWords\v201802\cm\SortOrder;
use Google\AdsApi\AdWords\v201802\cm\AdGroupService;
use Google\AdsApi\AdWords\v201802\cm\Predicate;
use Google\AdsApi\AdWords\v201802\cm\PredicateOperator;
use App\Model\DbInterface\AdWords\AdwordsCampaignTable;
use App\Model\DbInterface\AdWords\AdwordsAdGroupTable;

/**
 * Description of AdwordsAdSets
 *
 * @author davidkosvica
 */
class AdwordsAdGroups extends AdwordsAuthorization {

	const PAGE_LIMIT = 500;
	
	/**
	 * @var App\Model\DbInterface\AdWords\AdwordsCampaignTable
	 */
	private $adWordsCampaignTable;
	
	/**
	 * @var App\Model\DbInterface\AdWords\AdwordsAdGroupTable
	 */
	private $adWordsAdGroupsTable;
	
	public function __construct(Nette\Database\Context $database, $refreshToken) {
		parent::__construct($refreshToken);
		$this->adWordsCampaignTable = new AdwordsCampaignTable($database);
		$this->adWordsAdGroupsTable = new AdwordsAdGroupTable($database);
    }
	
	/**
	 * Vlozi reklamni skupiny kampani z adwords do db, kde tabulka se skupinami
	 * obsahuje cizi klic na tabulku kampani
	 * 
	 * @param array $campaigns
	 */
	public function setAdGroupsForCampaigns($campaigns, $customerId) {
		$this->buildSession($customerId);
		/* @var $campaign Google\AdsApi\AdWords\v201708\cm\Campaign */
		foreach ($campaigns as $campaign) {
			$adGroups = $this->getAdGroupsFromCampaign($campaign->getId());
			foreach ($adGroups as $adGroup) {
				$this->putAdGroupToDb($adGroup, $campaign->getId());
			}
		}
	}
	
	private function putAdGroupToDb($adGroup, $campaignId) {
		$adGroupId = $adGroup->getId();
		$adGroupName = $adGroup->getName();
		
		$primaryKeyCampaign = $this->adWordsCampaignTable
			->getPkBaseOnCampaignId($campaignId);
		
		try {
			$this->adWordsAdGroupsTable->insertAdGroup($primaryKeyCampaign, 
				$adGroupName, $adGroupId);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	private function getAdGroupsFromPage($page) {
		$adGroups = array();
		foreach ($page->getEntries() as $adGroup) {
			$adGroups[] = $adGroup;
		}
		return $adGroups;
	}
	
	private function getAdGroupsFromCampaign($campaignId) {
		// Vytvorim si instanci adword sluzby
		// $adWordsServices = new AdWordsServices();
		// ziskam sluzbu pro praci s reklamnimi kampanemi
		$adGroupService = $this->getAdwordService(AdGroupService::class);
		
		// Create a selector to select all ad groups for the specified campaign.
		$selector = new Selector();
		$selector->setFields(['Id', 'Name', 'CampaignId', 'CampaignName']);
		$selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
		$selector->setPredicates([new Predicate('CampaignId', PredicateOperator::IN, [$campaignId])]);
		$selector->setPaging(new Paging(0, self::PAGE_LIMIT));

		$totalNumEntries = 0;
		// Vytvorim si pole reklamnich skupin
		$adGroups = array();
		do {
			// Retrieve ad groups one page at a time, continuing to request pages
			// until all ad groups have been retrieved.
			$page = $adGroupService->get($selector);
			
			// Pokud neni vracena stranka prazdna proved
			if ($page->getEntries() !== null) {
				// Pri kazde iteraci nove stranky, si zjistim pocet
				// reklamnich skupin v kapmpani
				$totalNumEntries = $page->getTotalNumEntries();
				// Vlozim postupne vsechny reklamni skupiny do pole
				// reklamnich skupin
				$adGroups = array_merge($adGroups, $this->getAdGroupsFromPage($page));
			}

			// Nastavim startovni bod, od ktereho ctu reklamni skupiny, zvetseny
			// konstantu strankoveho limitu
			$selector->getPaging()->setStartIndex($selector->getPaging()->getStartIndex() + self::PAGE_LIMIT);
		
		// Pokracuj dokud startovni bod, od ktereho se cte, neni vetsi
		// nebo roven celkovemu poctu reklamnich skupin v kampani
		} while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
		return $adGroups;
	}
}
