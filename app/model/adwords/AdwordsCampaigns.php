<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;
use Nette;
use App\Model\Adwords\AdwordsAuthorization;
use Google\AdsApi\AdWords\v201802\cm\CampaignService;
use Google\AdsApi\AdWords\v201802\cm\OrderBy;
use Google\AdsApi\AdWords\v201802\cm\Paging;
use Google\AdsApi\AdWords\v201802\cm\Selector;
use Google\AdsApi\AdWords\v201802\cm\SortOrder;
use App\Model\DbInterface\AdWords\AdwordsAccountTable;
use App\Model\DbInterface\AdWords\AdwordsCampaignTable;

/**
 * Description of AdwordsCampaigns
 *
 * @author davidkosvica
 */
class AdwordsCampaigns extends AdwordsAuthorization {
	
	const PAGE_LIMIT = 500;
	
	private $campaigns;
	
	/**
	 *
	 * @var App\Model\DbInterface\AdWords\AdwordsAccountTable
	 */
	private $adWordsAccountTable;
	
	/**
	 *
	 * @var App\Model\DbInterface\AdWords\AdwordsCampaignTable
	 */
	private $adWordsCampaignTable;
	
	public function __construct(Nette\Database\Context $database, $refreshToken) {
		parent::__construct($refreshToken);
		$this->adWordsAccountTable = new AdwordsAccountTable($database);
		$this->adWordsCampaignTable = new AdwordsCampaignTable($database);
    }
	
	/** 
	 * @return type Description  
	 */
	private function getCampaignsForCustomer($idCustomer) {
        $this->buildSession($idCustomer);
		$campaign = $this->getCampaigns();
		return $campaign;
	}
	
	/** 
	 * @param \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomer[] $managedCustomers
	 * @return Google\AdsApi\AdWords\v201708\cm\Campaign[]
	 */
	public function getCampaignsOfManagedCustomers($managedCustomers) {
		foreach ($managedCustomers as $managedCustomer) {
			$idCustomer = $managedCustomer->getCustomerId();
			$campaigns[strval($idCustomer)] = $this->getCampaignsForCustomer($idCustomer);
		} 
        return $campaigns;
	}
	
	/**
	 * Vlozi kampane patrici uzivatelum z adwords do db, kde v tabulce
	 * kampani je cizi klic na tabulku uzivatelu 
	 * 
	 * @param \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomer[] $managedCustomers
	 */
	public function setCampaignsForCustomers($managedCustomers) {
		foreach ($managedCustomers as $managedCustomer) {
            $idCustomer = $managedCustomer->getCustomerId();
            $campaigns = $this->getCampaignsForCustomer($idCustomer);
			$idAdwordsAccount = $this->adWordsAccountTable->getPkBaseOnCustomerId($idCustomer);
			if (!empty($campaigns) && $idAdwordsAccount) {
				$this->putCampaignToDb($campaigns, $idAdwordsAccount);
			} 
		} 
	}
	
	private function putCampaignToDb($campaigns, $idAdwordsAccount) {
		foreach ($campaigns as $campaign) {
			$idCampaign = $campaign->getId();
			try {
				$this->adWordsCampaignTable
					->insertCampaign($idCampaign, $idAdwordsAccount, $campaign);
			} catch (Nette\Database\UniqueConstraintViolationException $e) {}
		}
	}
	
	/**
	 * 
	 * @return Google\AdsApi\AdWords\v201708\cm\Campaign[]
	 */
	private function getCampaigns() {
		if ($this->campaigns && array_key_exists(strval($this->userId), $this->campaigns)) {
            $campaigns = $this->campaigns[$this->userId];
		} else {
			$campaignService = $this->getAdwordService(CampaignService::class);
			$selector = new Selector();
			$selector->setFields(['Id', 'Name']);
			$selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
			$selector->setPaging(new Paging(0, self::PAGE_LIMIT));

			$totalNumEntries = 0;
			$campaigns = array();
			do {
			  // Make the get request.
			  $page = $campaignService->get($selector);

			  if ($page->getEntries() !== null) {
				$totalNumEntries = $page->getTotalNumEntries();
				foreach ($page->getEntries() as $campaign) {
					$campaigns[] = $campaign;
				}
			  }

			  // Advance the paging index.
			  $selector->getPaging()->setStartIndex(
				  $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT);
			} while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
			
			$this->campaigns[$this->userId] = $campaigns;
		}
		
		return $campaigns;
	}
}
