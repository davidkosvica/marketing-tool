<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;
use App\Model\Adwords\AdwordsAuthorization;

use App\Model\DbInterface\AdWords\AdwordsCampaignStatsTable;
use App\Model\DbInterface\AdWords\AdwordsAdGroupStatsTable;
use App\Model\DbInterface\AdWords\AdwordsKeywordStatsTable;
use Nette\Database\Context;
//use Google\AdsApi\AdWords\AdWordsSession;
//use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\v201802\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDownloader;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201802\cm\Predicate;
use Google\AdsApi\AdWords\v201802\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201802\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201802\cm\Selector;
//use Google\AdsApi\Common\OAuth2TokenBuilder;

/**
 * Description of DownloadCriteriaReportWithSelector
 *
 * @author davidkosvica
 */
class DownloadCriteriaReportWithSelector extends AdwordsAuthorization {
	
	private $filePath;
	private $adwordsCampaignStatsTable;
	private $adwordsAdGroupStatsTable;
	private $adwordsKeywordStatsTable;
	
	function __construct($refreshToken, Context $database) {
		parent::__construct($refreshToken);
		$this->adwordsCampaignStatsTable = new AdwordsCampaignStatsTable($database);
		$this->adwordsAdGroupStatsTable = new AdwordsAdGroupStatsTable($database);
		$this->adwordsKeywordStatsTable = new AdwordsKeywordStatsTable($database);
	}
	
	public function setFilePath($filePath) {
		$this->filePath = $filePath;
	}
	
	public function downloadCampaignReport() {
		$selector = new Selector();
		$selector->setFields(
			['CampaignId',
			'Date', 
			'Impressions', 
			'Clicks', 
			'AverageCpc',
			'Ctr', 
			'Conversions', 
			'AveragePosition', 
			'ConversionRate',
			]);
		
		// Use a predicate to filter out paused criteria (this is optional).
//		$selector->setPredicates([
//			new Predicate('Status', PredicateOperator::NOT_IN, ['PAUSED'])]);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector($selector);
		$reportDefinition->setReportName(
			'Criteria performance report #' . uniqid());
		$reportDefinition->setDateRangeType(
			ReportDefinitionDateRangeType::LAST_14_DAYS);
		$reportDefinition->setReportType(ReportDefinitionReportType::CAMPAIGN_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::CSV);
		
		// Download report.
		$reportDownloader = new ReportDownloader($this->getSession());
		// Optional: If you need to adjust report settings just for this one
		// request, you can create and supply the settings override here. Otherwise,
		// default values from the configuration file (adsapi_php.ini) are used.
		$reportSettingsOverride = (new ReportSettingsBuilder())
			->includeZeroImpressions(true)
			->useRawEnumValues(true)
			->skipReportHeader(true)
			->skipReportSummary(true)
			->build();
		$reportDownloadResult = $reportDownloader->downloadReport(
			$reportDefinition, $reportSettingsOverride);
		
//		\Tracy\Debugger::$maxDepth = 5;
//		\Tracy\Debugger::$maxLength = 1000000;
//		\Tracy\Debugger::barDump(get_class_methods($reportDownloadResult));
//		\Tracy\Debugger::barDump($reportDownloadResult->getAsString());
		
		return $reportDownloadResult->getAsString();
	}
	
	public function downloadAdGroupReport() {
		$selector = new Selector();
		$selector->setFields(
			['AdGroupId',
			'Date', 
			'Impressions', 
			'Clicks', 
			'AverageCpc',
			'Ctr', 
			'Conversions', 
			'AveragePosition', 
			'ConversionRate',
			]);
		
		
		// Use a predicate to filter out paused criteria (this is optional).
//		$selector->setPredicates([
//			new Predicate('Status', PredicateOperator::NOT_IN, ['PAUSED'])]);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector($selector);
		$reportDefinition->setReportName(
			'Criteria performance report #' . uniqid());
		$reportDefinition->setDateRangeType(
			ReportDefinitionDateRangeType::LAST_14_DAYS);
		$reportDefinition->setReportType(ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::CSV);
		
		// Download report.
		$reportDownloader = new ReportDownloader($this->getSession());
		// Optional: If you need to adjust report settings just for this one
		// request, you can create and supply the settings override here. Otherwise,
		// default values from the configuration file (adsapi_php.ini) are used.
		$reportSettingsOverride = (new ReportSettingsBuilder())
			->includeZeroImpressions(true)
			->useRawEnumValues(true)
			->skipReportHeader(true)
			->skipReportSummary(true)
			->build();
		$reportDownloadResult = $reportDownloader->downloadReport(
			$reportDefinition, $reportSettingsOverride);
		
		return $reportDownloadResult->getAsString();
	}
	
	public function downloadKeywordReport() {
		$selector = new Selector();
		$selector->setFields(
			['Id',
			'Date', 
			'Impressions', 
			'Clicks', 
			'AverageCpc',
			'Ctr', 
			'Conversions', 
			'AveragePosition', 
			'ConversionRate',
			]);
		
		// Use a predicate to filter out paused criteria (this is optional).
//		$selector->setPredicates([
//			new Predicate('Status', PredicateOperator::NOT_IN, ['PAUSED'])]);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector($selector);
		$reportDefinition->setReportName(
			'Criteria performance report #' . uniqid());
		$reportDefinition->setDateRangeType(
			ReportDefinitionDateRangeType::LAST_14_DAYS);
		$reportDefinition->setReportType(ReportDefinitionReportType::KEYWORDS_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::CSV);
		
		// Download report.
		$reportDownloader = new ReportDownloader($this->getSession());
		// Optional: If you need to adjust report settings just for this one
		// request, you can create and supply the settings override here. Otherwise,
		// default values from the configuration file (adsapi_php.ini) are used.
		$reportSettingsOverride = (new ReportSettingsBuilder())
			->includeZeroImpressions(true)
			->useRawEnumValues(true)
			->skipReportHeader(true)
			->skipReportSummary(true)
			->build();
		$reportDownloadResult = $reportDownloader->downloadReport(
			$reportDefinition, $reportSettingsOverride);
		
		return $reportDownloadResult->getAsString();
	}
	
	public function putCampaignReportInDb($report) {
		if (!empty($report)) {
			foreach ($report as $reportOfDay) {
				$this->adwordsCampaignStatsTable->insertCampaignStats($reportOfDay);
			}
		}
	}
	
	public function putAdGroupReportInDb($report) {
		if (!empty($report)) {
			foreach ($report as $reportOfDay) {
				$this->adwordsAdGroupStatsTable->insertAdGroupStats($reportOfDay);
			}
		}
	}
	
	public function putKeywordReportInDb($report) {
		if (!empty($report)) {
			foreach ($report as $reportOfDay) {
				$this->adwordsKeywordStatsTable->insertKeywordStats($reportOfDay);
			}
		}
	}
		
}
