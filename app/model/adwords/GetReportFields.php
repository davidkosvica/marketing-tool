<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;
//use Google\AdsApi\AdWords\AdWordsServices;
//use Google\AdsApi\AdWords\AdWordsSession;
//use Google\AdsApi\AdWords\AdWordsSessionBuilder;
//use Google\AdsApi\AdWords\v201802\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201802\cm\ReportDefinitionService;
//use Google\AdsApi\Common\OAuth2TokenBuilder;

/**
 * Description of GetReportFields
 *
 * @author davidkosvica
 */
class GetReportFields extends AdwordsAuthorization {
	
	const PAGE_LIMIT = 500;

	function __construct($refreshToken) {
		parent::__construct($refreshToken);
		$this->buildSession();
	}
	
	public function get($reportType) {
		/* @var $reportDefinitionService \Google\AdsApi\AdWords\v201708\cm\ReportDefinitionService */
		$reportDefinitionService = $this
			->getAdwordService(ReportDefinitionService::class);
		// The type of the report to get fields for.

		// Get report fields of the report type.
		$reportDefinitionFields = $reportDefinitionService
			->getReportFields($reportType );
		
		$formattedOutput = "The report type {$reportType} contains the following fields:\n";
		
		foreach ($reportDefinitionFields as $reportDefinitionField) {
			/* @var $reportDefinitionField \Google\AdsApi\AdWords\v201708\cm\ReportDefinitionField */
			$formattedOutput .= "{$reportDefinitionField->getFieldName()} "
			. "({$reportDefinitionField->getFieldType()}) ";
			if ($reportDefinitionField->getEnumValues() !== null) {
				$formattedOutput .= " := [" . implode(', ', $reportDefinitionField->getEnumValues()) . "]";
			}
			$formattedOutput .= "\n";
		}
		
		return $formattedOutput;
	}
}
