<?php 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model\Adwords;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;

/**
 * Description of adwords_authorization
 *
 * @author davidkosvica
 */
class AdwordsAuthorization {
	
	private $refreshToken;
	private $session;
	private $credentials;
	private $adwordServices;
	protected $userId;
	
	const CONFIG_FILE = __DIR__ . "/../../config/adsapi_php.ini";
    
	const CLIENT_ID = "682904102686-thrrbn97p4hlbi4nou5i4prma0h013nn.apps."
					. "googleusercontent.com";
	
	const CLIENT_SECRET = "F23eNRaHCujuUL6Lz475Lpdg";
	
	public function __construct($refreshToken) {
		$this->adwordServices = new AdWordsServices();
		$this->setRefreshToken($refreshToken);
//		$this->buildSession();
    }
	
	/**
	 * @param int $refreshToken
	 */
	public function setRefreshToken($refreshToken) {
		$this->refreshToken = $refreshToken;
	}
	
	/**
	 * @return int $refreshToken
	 */
	public function getRefreshToken() {
		return $this->refreshToken;
	}
	
	protected function buildOAuth2Credential() {
		$refreshToken = $this->getRefreshToken();
		
		$oAuth2Credential = (new OAuth2TokenBuilder())
			->withClientId(self::CLIENT_ID)
			->withClientSecret(self::CLIENT_SECRET)
			->withRefreshToken($refreshToken)
			->build();
		
		$this->credentials = $oAuth2Credential;
	}
	
	protected function getAdwordService($service) {
		return $this->adwordServices->get($this->session, $service);
	}
	
	/*
	 * Vytvori session v adword api pro daneho uzivatele, pripadne session
	 * bez uzivatele.
	 */
	public function buildSession($customerId = null) {
        if (!$this->credentials) {
			$this->buildOAuth2Credential();
		}
		
		if ($customerId == null) {
			$session = (new AdWordsSessionBuilder())
				->fromFile(self::CONFIG_FILE)
				->withOAuth2Credential($this->credentials)
				->build();
		} else {
			$session = (new AdWordsSessionBuilder())
				->fromFile(self::CONFIG_FILE)
				->withOAuth2Credential($this->credentials)
				->withClientCustomerId($customerId)
				->build();
				$this->userId = $customerId;
		}
		
		$this->session = $session;
		return $session;
	}
	
	public function getSession() {
		return $this->session;
	}
}
