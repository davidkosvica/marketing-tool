<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;

use App\Model\Adwords\AdwordsAuthorization;

//use App\Model\DbInterface\AdWords\AdwordsCampaignStatsTable;
//use App\Model\DbInterface\AdWords\AdwordsAdGroupStatsTable;
use App\Model\DbInterface\AdWords\AdwordsKeywordTable;
use Nette\Database\Context;
//use Google\AdsApi\AdWords\AdWordsSession;
//use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\v201802\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDownloader;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
//use Google\AdsApi\AdWords\v201802\cm\Predicate;
//use Google\AdsApi\AdWords\v201802\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201802\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201802\cm\Selector;
//use Google\AdsApi\Common\OAuth2TokenBuilder;

/**
 * Description of AdwordsKeywords
 *
 * @author david
 */
class AdwordsKeywords extends AdwordsAuthorization  {
	
	private $adwordsKeywordsTable;
	
	function __construct(Context $database, $refreshToken) {
		parent::__construct($refreshToken);
		$this->adwordsKeywordsTable = new AdwordsKeywordTable($database);
	}
	
	public function getKeywords() {
		$selector = new Selector();
		$selector->setFields(
			['AdGroupId', 'Id', 'Criteria']);
		
		// Use a predicate to filter out paused criteria (this is optional).
//		$selector->setPredicates([
//			new Predicate('Status', PredicateOperator::NOT_IN, ['PAUSED'])]);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector($selector);
		$reportDefinition->setReportName(
			'Criteria performance report #' . uniqid());
		$reportDefinition->setDateRangeType(
			ReportDefinitionDateRangeType::LAST_14_DAYS);
		$reportDefinition->setReportType(ReportDefinitionReportType::KEYWORDS_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::CSV);
		
		// Download report.
		$reportDownloader = new ReportDownloader($this->getSession());
		// Optional: If you need to adjust report settings just for this one
		// request, you can create and supply the settings override here. Otherwise,
		// default values from the configuration file (adsapi_php.ini) are used.
		$reportSettingsOverride = (new ReportSettingsBuilder())
			->includeZeroImpressions(true)
			->useRawEnumValues(true)
			->skipReportHeader(true)
			->skipReportSummary(true)
			->build();
		$reportDownloadResult = $reportDownloader->downloadReport(
			$reportDefinition, $reportSettingsOverride);
		
		return $reportDownloadResult->getAsString();
	}
	
	function putKeywordsInDb($keywords) {
		foreach ($keywords as $keyword) {
			$this->adwordsKeywordsTable->insertKeyword($keyword);
		}
	}
}
