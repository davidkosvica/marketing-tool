<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;

use Nette;
//use Google\AdsApi\AdWords\v201708\cm\OrderBy;
//use Google\AdsApi\AdWords\v201708\cm\Paging;
//use Google\AdsApi\AdWords\v201708\cm\Selector;
//use Google\AdsApi\AdWords\v201708\cm\SortOrder;
//use Google\AdsApi\AdWords\v201708\mcm\ManagedCustomerService;
//use Google\AdsApi\AdWords\v201708\mcm\CustomerService;
use Google\AdsApi\AdWords\v201802\cm\OrderBy;
use Google\AdsApi\AdWords\v201802\cm\Paging;
use Google\AdsApi\AdWords\v201802\cm\Selector;
use Google\AdsApi\AdWords\v201802\cm\SortOrder;
use Google\AdsApi\AdWords\v201802\mcm\ManagedCustomerService;
use Google\AdsApi\AdWords\v201802\mcm\CustomerService;
use App\Model\Adwords\AdwordsAuthorization;

use App\Model\DbInterface\Google\GoogleAccountTable;
use App\Model\DbInterface\Google\UserToGoogleAccount;
use App\Model\DbInterface\AdWords\AdwordsAccountTable;
use App\Model\DbInterface\AdWords\AdwordsAccountToManagerAccountTable;

use Nette\Database\Context;
/**
 * Description of AdwordsCustomers
 *
 * @author davidkosvica
 */
class AdwordsCustomers extends AdwordsAuthorization {
	
	/**
	 * @var \Google\AdsApi\AdWords\v201708\mcm\Customer[] 
	 */
	private $directlyAccessibleCustomers;
	
	/**
	 * @var Google\AdsApi\AdWords\v201609\mcm\ManagedCustomer
	 */
	private $rootAccount;
	
	/**
	 * @var Google\AdsApi\AdWords\v201609\mcm\ManagedCustomer[]
	 */
	private $customerIdsToAccounts;
	
	/**
	 * @var various
	 */
	private $customerIdsToChildLinks;
	
	/**
	 * @var App\Model\DbInterface\Google\GoogleAccountTable
	 */
	private $googleAccountTable;
	
	/**
	 * @var App\Model\DbInterface\AdWords\AdwordsAccountTable
	 */
	private $adwordsAccountTable;
	
	/**
	 * @var App\Model\DbInterface\Google\UserToGoogleAccount;
	 */
	private $userToGoogleAccount;
	
	/**
	 * @var App\Model\DbInterface\AdWords\AdwordsAccountToManagerAccountTable
	 */
	private $adwordsAccountToManagerAccountTable;
	
	const PAGE_LIMIT = 500;
	
	public function __construct(Context $database, $refreshToken) {
		parent::__construct($refreshToken);
		$this->googleAccountTable = new GoogleAccountTable($database);
		$this->adwordsAccountTable = new AdwordsAccountTable($database);
		$this->adwordsAccountToManagerAccountTable = new AdwordsAccountToManagerAccountTable($database);
		$this->userToGoogleAccount = new UserToGoogleAccount($database);
		
		$this->buildSession();
    }
	
	public function setRefreshToken($refreshToken) {
		parent::setRefreshToken($refreshToken);
	}
	
	/**
	 * @return \Google\AdsApi\AdWords\v201708\mcm\Customer[]
	 */
	public function getDirectlyAccessibleCustomers() {
		$customerService = $this->getAdwordService(CustomerService::class);
		if (!$this->directlyAccessibleCustomers) {
			$this->directlyAccessibleCustomers = $customerService->getCustomers();
		} 
		
		return $this->directlyAccessibleCustomers;
	}
	
	// Vrati strom uzivatelu zacinajici korenovym uzivatelem, jinak false
	private function getCustomerHierarchy() {
		if (!($this->rootAccount && $this->customerIdsToAccounts && $this->customerIdsToChildLinks)) {
			$managedCustomerService = $this->getAdwordService(ManagedCustomerService::class);
			// Create selector.
			$selector = new Selector();
			$selector->setFields(['CustomerId', 'Name']);
			$selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
			$selector->setPaging(new Paging(0, self::PAGE_LIMIT));

			// Maps from customer IDs to accounts and links.
			$customerIdsToAccounts = [];
			$customerIdsToChildLinks = [];
			$customerIdsToParentLinks = [];

			$totalNumEntries = 0;
			do {
			  // Make the get request.
			  /* @var $page \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomerPage */	
			  $page = $managedCustomerService->get($selector);
			  // Create links between manager and clients.
			  if ($page->getEntries() !== null) {
				$totalNumEntries = $page->getTotalNumEntries();
				if ($page->getLinks() !== null) {
					/* @var $link \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomerLink */
					foreach ($page->getLinks() as $link) {
						// Cast the indexes to string to avoid the issue when 32-bit PHP
						// automatically changes the IDs that are larger than the 32-bit max
						// integer value to negative numbers.
						$managerCustomerId = strval($link->getManagerCustomerId());
						$customerIdsToChildLinks[$managerCustomerId][] = $link;
						$clientCustomerId = strval($link->getClientCustomerId());
						$customerIdsToParentLinks[$clientCustomerId] = $link;
					}
				}

				/* @var $account \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomer */
				foreach ($page->getEntries() as $account) {
					$customerIdsToAccounts[strval($account->getCustomerId())] = $account;
				}
			  }
			  // Advance the paging index.
			  $selector->getPaging()->setStartIndex(
				$selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
			  );
			} while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

			// Find the root account.
			$rootAccount = null;
			foreach ($customerIdsToAccounts as $account) {
			  if (!array_key_exists($account->getCustomerId(), $customerIdsToParentLinks)) {
				$rootAccount = $account;
				break;
			  }
			}

			if ($rootAccount !== null) {

				// $rootAccount: objekt "ManagedCustomer", 
				// ktery reprezentuje korenovy ucet
				// 
				// $customerIdsToAccounts: Pole objektu "ManagedCustomer", 
				// zacinajici korenovym uctem a dalsimi ucty ktere korenovy ucet
				// spravuje.
				// 
				// $customerIdsToChildLinks: Pole objektu "ManagedCustomerLink", 
				// ktera mapuji hierarchii vztahu mezi ucty
				$this->rootAccount = $rootAccount;
				$this->customerIdsToAccounts = $customerIdsToAccounts;
				$this->customerIdsToChildLinks = $customerIdsToChildLinks;
				$hierarchy = [$rootAccount, $customerIdsToAccounts, $customerIdsToChildLinks];
				
			} else {
				$hierarchy = false;
			}
			
		} else {
			$hierarchy = [$this->rootAccount, $this->customerIdsToAccounts,
					$this->customerIdsToChildLinks];
		}
		return $hierarchy;
	}
	
	/** 
	 * Druha pozice z hierarchie obsahuje pole id_uctu => ucet
	 * @return \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomer[]
	 */
	public function getCustomers() {
		return $this->getCustomerHierarchy()[1];
	}
	
	public function getCustomersLinks() {
		return $this->getCustomerHierarchy()[2];
	}
	
	/**
	 * 
	 * @param integer $userId
	 * @param \Google\AdsApi\AdWords\v201708\mcm\Customer $customer
	 */
	public function putAdwordCustomersToDb($userId, $customer) {
		$this->buildSession($customer->getCustomerId());
		$this->getCustomerHierarchy();
		$managedCustomers = $this->getCustomers();
		$managedCustomerLinks = $this->getCustomersLinks();
		
		$idGoogleAccount = $this->userToGoogleAccount->getUsersActivatedGoogleAccountPk($userId);
		
		foreach ($managedCustomers as $managedCustomer) {
			$this->putAdwordCustomerToDb($managedCustomer, $idGoogleAccount);
		}
		
		$this->setAdwordCustomerRelationships($managedCustomerLinks);
	}
	
	private function setAdwordCustomerRelationships($managersRelationships) {
		
		foreach ($managersRelationships as $managerRelationship) {
			foreach ($managerRelationship as $customerLink) {
				$managerAccountPk = $this->adwordsAccountTable
					->getPkBaseOnCustomerId($customerLink->getManagerCustomerId());
				
				$customerAccountPk = $this->adwordsAccountTable
					->getPkBaseOnCustomerId($customerLink->getClientCustomerId());
				
				$this->putAdwordLinkToDb($customerAccountPk, $managerAccountPk);
			}
		}
	}
	
	private function putAdwordLinkToDb($customerPk, $managerPk) {
		try {
			$this->adwordsAccountToManagerAccountTable
				->insertRelationship($managerPk, $customerPk);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	/**
	 * @param \Google\AdsApi\AdWords\v201708\mcm\ManagedCustomer $managedCustomer
	 */
	private function putAdwordCustomerToDb($managedCustomer, $idGoogleAccount) {
		try {
			// pokud se jedna o root account
			if ($managedCustomer->getCustomerId() == $this->rootAccount
				->getCustomerId()) {
				$this->adwordsAccountTable
					->insertManagerAdwordAccount($managedCustomer, 
						$idGoogleAccount);
			// pokud se nejedna o root account
			} else {
				$this->adwordsAccountTable
					->insertAdwordAccount($managedCustomer);
			}
		} catch (Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	/**
	 * @returns \Nette\Database\Table\ActiveRow[]
	 */
	public function getAdwordsCustomersIdsFromDb() {
		return $this->adwordsAccountTable->getAdwordsAccountsIds();
	}
}
