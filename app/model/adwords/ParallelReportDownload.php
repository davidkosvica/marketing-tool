<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Adwords;

use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\Reporting\v201708\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201708\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201708\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201708\ReportDownloader;
use Google\AdsApi\AdWords\v201708\cm\ApiException;
use Google\AdsApi\AdWords\v201708\cm\Paging;
use Google\AdsApi\AdWords\v201708\cm\Predicate;
use Google\AdsApi\AdWords\v201708\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201708\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201708\cm\Selector;
use Google\AdsApi\AdWords\v201708\mcm\ManagedCustomerService;

/**
 * Description of ParallelReportDownload
 *
 * @author davidkosvica
 */
class ParallelReportDownload extends AdwordsAuthorization {
	// Timeout between retries in seconds.
	const BACKOFF_FACTOR = 5;

	// Maximum number of retries for 500 errors.
	const MAX_RETRIES = 5;

	// The number of entries per page of the results.
	const PAGE_LIMIT = 500;

	function getParallelReport($customerId) {
		$session = $this->buildSession($customerId);
		
		$selector = new Selector();
		$selector->setFields(
			['CampaignId', 'AdGroupId', 'Impressions', 'Clicks', 'Cost']);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector($selector);
		$reportDefinition->setReportName('Custom ADGROUP_PERFORMANCE_REPORT');
		$reportDefinition->setDateRangeType(
			ReportDefinitionDateRangeType::ALL_TIME);
		$reportDefinition->setReportType(
			ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::XML);
		
		$customerIds = self::getAllManagedCustomerIds($session);
//		printf("Downloading reports for %d managed customers.\n",
//			count($customerIds));
		
		$successfulReports = [];
		$failedReports = [];
		
		$reportDir = __DIR__ . '/../../../temp';
		
		foreach ($customerIds as $customerId) {
			$filePath = sprintf('%s%sadgroup_%d.xml', $reportDir,
				DIRECTORY_SEPARATOR, $customerId);

			// Construct an API session for the specified client customer ID.
//			$session = $sessionBuilder->withClientCustomerId($customerId)->build();
			$session = $this->buildSession($customerId);
			$reportDownloader = new ReportDownloader($session);

			$retryCount = 0;
			$doContinue = true;
			
			do {	
				$retryCount++;
				try {
					// Optional: If you need to adjust report settings just for this one
					// request, you can create and supply the settings override here.
					// Otherwise, default values from the configuration file
					// (adsapi_php.ini) are used.
					$reportSettingsOverride = (new ReportSettingsBuilder())
						->includeZeroImpressions(false)
						->build();
					$reportDownloadResult = $reportDownloader->downloadPerformanceStatistics(
						$reportDefinition, $reportSettingsOverride);
					$reportDownloadResult->saveToFile($filePath);
//					printf(
//						"Report for client customer ID %d successfully downloaded to: "
//							. "%s\n",
//						$customerId,
//						$filePath
//					);
					$successfulReports[$customerId] = $filePath;
					$doContinue = false;
				} catch (ApiException $e) {
//					printf(
//						"Report attempt #%d for client customer ID %d was not downloaded"
//							. " due to: %s\n",
//						$retryCount,
//						$customerId,
//						$e->getMessage()
//					);

					// If this is a server error, retry up to the defined maximum number
					// of retries.
					if ($e->getErrors() === null && $retryCount < self::MAX_RETRIES) {
						$sleepTime = $retryCount * self::BACKOFF_FACTOR;
//						printf(
//							"Sleeping %d seconds before retrying report for client customer"
//								. " ID %d.\n",
//							$sleepTime,
//							$customerId
//						);
						sleep($sleepTime);
					} else {
//						printf(
//							"Report request failed for client customer ID %d.\n",
//							$customerId
//						);
						$failedReports[$customerId] = $filePath;
						$doContinue = false;
					}
				}		
			} while ($doContinue === true);
		}
	}
	

	/**
	 * Retrieves all the customer IDs under a manager account.
	 *
	 * To set clientCustomerId to any manager account you want to get
	 * reports for its client accounts, use `AdWordsSessionBuilder` to
	 * create new session.
	 */
	private function getAllManagedCustomerIds(AdWordsSession $session) {
//		$managedCustomerService =
//			$adWordsServices->get($session, ManagedCustomerService::class);
		
		$managedCustomerService = 
			$this->getAdwordService(ManagedCustomerService::class);
		
		$selector = new Selector();
		$selector->setFields(['CustomerId']);
		$selector->setPaging(new Paging(0, self::PAGE_LIMIT));
		$selector->setPredicates([new Predicate('CanManageClients',
			PredicateOperator::EQUALS, ['false'])]);

		$customerIds = [];
		do {
			$page = $managedCustomerService->get($selector);
			if ($page->getEntries() !== null) {
					$totalNumEntries = $page->getTotalNumEntries();
				foreach ($page->getEntries() as $customer) {
					$customerIds[] = $customer->getCustomerId();
				}
			}
			$selector->getPaging()->setStartIndex(
				$selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
			);
		} while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

		return $customerIds;
    }
}
