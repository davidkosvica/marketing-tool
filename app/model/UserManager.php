<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use App\Model\DbInterface\UserTable;
use App\Model\DbInterface\Google\GoogleAccountTable;
use App\Model\DbInterface\Google\GoogleAccessTokenTable;
use App\Model\DbInterface\Google\GoogleRefreshTokenTable;
use App\Model\DbInterface\Facebook\FacebookAccountTable;
use App\Model\DbInterface\Facebook\UserToFbAccount;
use App\Model\DbInterface\Google\UserToGoogleAccount;
use App\Model\Sklik\SklikAdAccounts;
use App\Model\DbInterface\Sklik\UserToSklikAccountTable;
use App\Model\Sklik\BadSessionException;

/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;
		
	/** @var App\Model\DbInterface\UserTable */
	private $userTable;
	
	/** @var App\Model\DbInterface\Google\GoogleAccountTable */
	private $googleAccountTable;
	
	/** @var \App\Model\DbInterface\Google\GoogleAccessTokenTable */
	private $googleAccessTokenTable;
	
	/** @var \App\Model\DbInterface\Google\GoogleRefreshTokenTable */
	private $googleRefreshTokenTable;
	
	/** @var App\Model\DbInterface\Google\UserToGoogleAccount */
	private $userToGoogleAccount;
	
	/** @var App\Model\DbInterface\Facebook\FacebookAccountTable */
	private $facebookAccountTable;
	
	/** @var App\Model\DbInterface\Facebook\UserToFbAccount */
	private $userToFbAccount;
    
    /** @var UserToSklikAccountTable */
    private $userToSklikAccountTable;
    
    /** @var SklikAdAccounts */
    private $sklikAdAccounts;
	
	public function __construct(Nette\Database\Context $database)
	{
		$this->userTable = new UserTable($database);
		$this->googleAccountTable = new GoogleAccountTable($database);
		$this->userToGoogleAccount = new UserToGoogleAccount($database);
		$this->googleAccessTokenTable = new GoogleAccessTokenTable($database);
		$this->googleRefreshTokenTable = new GoogleRefreshTokenTable($database);
		$this->userToFbAccount = new UserToFbAccount($database);
		$this->facebookAccountTable = new FacebookAccountTable($database);
		$this->userToSklikAccountTable = new UserToSklikAccountTable($database);
        $this->sklikAdAccounts = new SklikAdAccounts($database);
	}
	
	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->userTable->getUserBaseOnLogin($username);

		if (!$row) {
				throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

		} elseif (!Passwords::verify($password, $row[UserTable::PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row[UserTable::PASSWORD_HASH])) {
			$row->update([UserTable::PASSWORD_HASH => Passwords::hash($password)]);
		}

		$arr = $row->toArray();
		unset($arr[UserTable::PASSWORD_HASH]);
		return new Nette\Security\Identity($row[UserTable::PRIMARY_KEY], $row[UserTable::ROLE], $arr);
	}
	
	public function RegisterFromFacebook($fbAccountId, $userObject, $userId)
	{

		$fbAccountIdPk = $this->facebookAccountTable->insertFacebookAccount($fbAccountId, $userObject);
		
		if (!$fbAccountIdPk) {
			$fbAccountIdPk = $this->facebookAccountTable->getFbAccountPkBaseOnFbId($fbAccountId);
		}
		
		try {
			$this->userToFbAccount->insertUserFbAccountLink($userId, $fbAccountIdPk);
		} catch (\Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	public function UpdateFacebookAccessToken($fbUserId, $userToken)
	{
		$this->facebookAccountTable->updateAccessToken($fbUserId, $userToken);
	}
	
	/*
	 * Funkce ziska pristupovy token uctu, ktery ma nastaveny priznak
	 * active (active je boolean datovy typ)
	 */
	public function getFacebookAccessToken($idUser)
	{
		$fbAccountId = $this->userToFbAccount->getUsersFbAccount($idUser);
		
		return $this->facebookAccountTable->getAccessToken($fbAccountId);
		
	}
	
	
	/*
	 * Funkce deaktivuje fb ucet
	 */
	public function deactivateUsersFacebookAccount($userId, $fbAccountId)
	{
		$this->userToFbAccount->disableFbAccount($userId, $fbAccountId);
	}
	
	/*
	 * Funkce aktivuje fb ucet
	 */
	public function activateUsersFacebookAccount($userId, $fbAccountId)
	{
		$this->userToFbAccount->enableFbAccount($userId, $fbAccountId);
	}
	
	/*
	 * Funkce deaktivuje google ucet
	 */
	public function deactivateUsersGoogleAccount($userId, $idFromGoogle)
	{
		$googleAccountPk = $this->googleAccountTable->getGoogleAccountPkBasedOnGoogleId($idFromGoogle);
		$this->userToGoogleAccount->disableGoogleAccount($userId, $googleAccountPk);
		
	}
	
	/*
	 * Funkce aktivuje google ucet
	 */
	public function activateUsersGoogleAccount($userId, $googleUserId)
	{
		$this->userToGoogleAccount->enableGoogleAccount($userId, $googleUserId);
	}
	
	public function DoesFbAccountExitsForUser($fbUserId, $userId) {
		$result = $this->userToFbAccount
			->getFbAccountByFbIdAndUserId($fbUserId, $userId);
		
		if ($result) {
			$found = true;
		} else {
			$found = false;
		}
		
		return $found;
	}
	
	/**
	 * parametr $userId je hodnota ze session, jedna se o primarni klic v 
	 * tabulce "users"
	 * parametr $userObject obsahuje informace z profilu google
	 */
	public function RegisterFromGoogle($userObject, $userId) {
		$googleAccountPk = $this->googleAccountTable->insertGoogleAccount($userObject);
		$this->userToGoogleAccount->insertUserGoogleAccountLink($userId, $googleAccountPk);
	}
	
	/*
	 * Metoda aktualizuje radek tabulky "google_access_token"
	 * na hodnotu v parametru $accessToken, pokud je v parametru
	 * pritomen klic "refresh_token" take dojde k vlozeni hodnoty 
	 * zmineneho klice do radku tabulky "google_refresh_token"
	 */
//	public function InsertUpdateGoogleAccessToken($idGoogleAccount, $accessToken) {
//		$this->googleAccessTokenTable->insertOrUpdateAccessToken($idGoogleAccount, $accessToken);
//	}
//	
//	public function InsertUpdateGoogleRefreshToken($pkGoogleAccount, $refreshToken) {
//		$this->googleRefreshTokenTable->insertOrUpdateRefreshTokenForAccount($pkGoogleAccount, $refreshToken);
//	}
	
	/**
	 * 
	 * @param type $userId
	 * @param type $accessToken
	 * 
	 * Metedo provede update acess tokenu prave prihlaseneho uzivatele,
	 * pomoci jeho id (id user objektu nette tridy)
	 */
	public function UpdateGoogleAccessToken($userId, $accessToken) {
		$googleAccountPk = $this->userToGoogleAccount->getUsersActivatedGoogleAccountPk($userId);
		if ($googleAccountPk) {
			$this->googleAccessTokenTable->insertOrUpdateAccessToken($googleAccountPk, $accessToken);
		}
	}
	
	public function UpdateGoogleRefreshToken($userId, $refreshToken) {
		$googleAccountPk = $this->userToGoogleAccount->getUsersActivatedGoogleAccountPk($userId);
		if ($googleAccountPk) {
			$this->googleRefreshTokenTable->insertOrUpdateRefreshTokenForAccount($googleAccountPk, $refreshToken);
		}
	}
	
	/*
	 * vrati access token uctu ktery je v tuto chvili aktivovany,
	 * pokud neni zadny ucet aktivovany vrati false, v jeden okamzik
	 * muze byt aktivni pouze jeden ucet (boolean priznak active urcuje
	 * zda je ucet aktivni)
	 */
	public function getGoogleAccessToken($idUser)
	{
		// 1. nejdriv zjistim id google uctu spadajiciho k userovi
		// 2. id google uctu pouziji v metode pro ziskani access tokenu
		//	  v tabulce GoogleAccessToken
		
		$accessToken = false;
		$googleAccountPk = $this->userToGoogleAccount->getUsersActivatedGoogleAccountPk($idUser);
		if ($googleAccountPk) {
			$accessToken = $this->googleAccessTokenTable->getAccessToken($googleAccountPk);
		}
		
		
		return $accessToken;
	}
	
	public function getGoogleRefreshToken($idUser) {
		$googleAccountPk = $this->userToGoogleAccount->getUsersActivatedGoogleAccountPk($idUser);
		return $this->googleRefreshTokenTable->getGoogleRefreshToken($googleAccountPk);
	}
	
	public function FindByGoogleId($idFromGoogle) {
		return $this->googleAccountTable->getGoogleAccountPkBasedOnGoogleId($idFromGoogle);
	}
	
	public function getUsersIds() {
		return $this->userTable->getAllUserIds();
	}

	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return void
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $password, $password_verify)
	{   
        $this->userTable->insertUser($username, $email, 
			$password, $password_verify);
	}
	
    public function insertUsersSklikAccount(array $accountAttributes, int $userId) : array {
        $sklikAccountPk = $this->sklikAdAccounts->insertAdAccount($accountAttributes);
        $this->userToSklikAccountTable->insert($sklikAccountPk, $userId);
        return [ 'user_id' => $userId, 'sklikAccountPk' => $sklikAccountPk];
    }
    
    public function activateUsersSklikAccount($userId, $sklikAccountPk) {
        $this->userToSklikAccountTable->setActive($userId, $sklikAccountPk);
    }
    
    public function deactivateUsersSklikAccount($userId, $sklikAccountPk) {
        $this->userToSklikAccountTable->setInactive($userId, $sklikAccountPk);
    }
    
    public function getUsersActivatedSklikAccountPk(int $userId) : int {
        return $this->userToSklikAccountTable->getUsersActiveAccount($userId);
    }
    
    public function renewUsersActivatedSklikSession(int $userId) : int {
        $success = false;
        $sklikAccountPk = $this->getUsersActivatedSklikAccountPk($userId);
        if ($sklikAccountPk) {
            $session = $this->sklikAdAccounts->getAccountSession($sklikAccountPk);
            try {
				$clientAttributes = $this->sklikAdAccounts->clientGet($session);
			} catch(BadSessionException $e) {
				$clientAttributes = false;
			} 
			
            if ($clientAttributes) {
                $session = $clientAttributes['session'];
                $this->sklikAdAccounts->setNewSession($session, $userId);
                $success = true;
            }
        }
        
        return $success;
    }
}