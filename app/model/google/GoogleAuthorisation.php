<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Google;
use App\Model\UserManager;
use Google_Client;

/**
 * Description of GoogleAuthorisation
 *
 * @author david
 */

class GoogleAuthorisation {

	const CLIENT_SERCRET_JSON = __DIR__ . "/../../config/client_secret_analytics.json";
	
	private $client;
	private $userManager;

	public function __construct(UserManager $userManager) {
		$this->userManager = $userManager;
		
		$this->client = new Google_Client();
		$this->client->setAuthConfig(self::CLIENT_SERCRET_JSON);
		$this->client->setAccessType('offline');
		$this->client->addScope('https://www.googleapis.com/auth/plus.login');
	}
	
	public function setRedirectUri($redirectUri) {
		$this->client->setRedirectUri($redirectUri);
	}
	
	public function addScope($scope) {
		$this->client->addScope($scope);
	}
	
	public function redirectToAuthPage() {
		$auth_url = $this->client->createAuthUrl();
		header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
	}
	
	public function getAccessToken($code) {
		$vysledek = $this->client->authenticate($code);
		$accessToken = $this->client->getAccessToken();
		$refreshToken = $this->client->getRefreshToken();
		return ['access_token' => $accessToken, 'refresh_token' => $refreshToken];
	}
	
	public function isAccessTokenStillValid($accessToken) {
		$this->client->setAccessToken($accessToken);
		if ($this->client->isAccessTokenExpired()) {
			return false;
		} else {
			return true;
		}
	}
	
	public function renewAccessToken($userId) {
		$refreshToken = $this->userManager->getGoogleRefreshToken($userId);
		$accessToken = $this->client->refreshToken($refreshToken);
		$this->userManager->UpdateGoogleAccessToken($userId, $accessToken);
		return $accessToken;
	}
	
	public function getClient() {
		return $this->client;
	}
	
	public function getUserId($accessToken) {
		$this->setAccessToken($accessToken);
		$oauth2 = new \Google_Service_Oauth2($this->client);
		$userInfo = $oauth2->userinfo->get();
		return $userInfo->{'id'};
	} 
	
	public function setAccessToken($accessToken) {
		$this->client->setAccessToken($accessToken);
	}
}
