<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use Nette;
use App\Model\Sklik\SklikAdAccounts;
use App\Model\Sklik\SklikCampaigns;
use App\Model\Sklik\SklikGroups;
use App\Model\Sklik\SklikKeywords;
use App\Model\UserManager;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\SklikCampaignStatistics;
use App\Model\Sklik\SklikGroupStatistics;
use App\Model\Sklik\SklikKeywordStatistics;
use App\Model\Sklik\SklikStatisticsGeneral;
use Nette\Database\Context;
use App\Model\Sklik\XmlRpcKeywords\Attribute;

/**
 * Description of SklikManager
 *
 * @author davidkosvica
 */
class SklikManager {
    use Nette\SmartObject;
    
    /** @var UserManager $userManager */
    private $userManager;
    
    /** @var SklikCampaigns $sklikCampaigns */
    private $sklikCampaigns;
    
    /** @var SklikAdAccounts $sklikAdAccounts */
    private $sklikAdAccounts;
	
    /** @var SklikGroups $sklikGroups */
    private $sklikGroups;
	
	/** @var SklikKeywords $sklikKeywords */
    private $sklikKeywords;
	
	/** @var SklikStatisticsGeneral $sklikStatisticsGeneral */
    private $sklikStatisticsGeneral;
	
	/** @var SklikCampaignStatistics $sklikCampaignStatistics */
    private $sklikCampaignStatistics;
	
	/** @var SklikGroupStatistics $sklikGroupStatistics */
    private $sklikGroupStatistics;
	
	/** @var SklikKeywordStatistics $sklikKeywordStatistics */
    private $sklikKeywordStatistics;
	
    public function __construct(Context $database, SklikAdAccounts $sklikAdAccounts, 
			SklikCampaigns $sklikCampaigns, SklikGroups $sklikGroups, 
			SklikKeywords $sklikKeywords, UserManager $userManager,
			SklikCampaignStatistics $sklikCampaignStatistics, SklikGroupStatistics $sklikGroupStatistics,
			SklikKeywordStatistics $sklikKeywordStatistics) {
        
        $this->sklikAdAccounts = $sklikAdAccounts;
        $this->sklikCampaigns = $sklikCampaigns;
        $this->sklikGroups = $sklikGroups;
        $this->sklikKeywords = $sklikKeywords;
		$this->userManager = $userManager;
		$this->sklikCampaignStatistics = $sklikCampaignStatistics;
		$this->sklikGroupStatistics = $sklikGroupStatistics;
		$this->sklikKeywordStatistics = $sklikKeywordStatistics;
		$this->sklikStatisticsGeneral = new SklikStatisticsGeneral();
		
	}
	
	private function downloadKeywords($groups, $session) {
		foreach ($groups as $group) {
			$groupIds[] = $group['id'];
		}
		
		$keywords = $this->sklikKeywords->getKeywords($session, $groupIds)['keywords'];
		$this->sklikKeywords->insertKeywordsIntoDb($keywords);
		
		return $keywords;
	}
	
	private function downloadGroups($campaigns, $session) {
		foreach ($campaigns as $campaign) {
			$campaignIds[] = $campaign[Attribute::ID];
		}
		
		$groups = $this->sklikGroups->getGroups($session, $campaignIds)['groups'];
		$this->sklikGroups->insertGroupsIntoDb($groups);
		
		return $groups;
	}
	
	private function downloadCampaigns(string $session, int $sklikAccountPk) {
		$campaigns = $this->sklikCampaigns->getCampaigns($session)['campaigns'];
		
		foreach ($campaigns as $campaign) {
			$this->sklikCampaigns->insertCampaignIntoDb($campaign, $sklikAccountPk);
			$campaignId = $campaign[Attribute::ID];
		}
		
		return $campaigns;
	}
	
	private function downloadMarketingTree($sklikAccountPk, $session) {
		$campaigns = $this->downloadCampaigns($session, $sklikAccountPk);
		$groups = $this->downloadGroups($campaigns, $session);
		$keywords = $this->downloadKeywords($groups, $session);
		
		return [
			'campaigns' => $campaigns,
			'groups' => $groups,
			'keywords' => $keywords,
		];
	}
	
	private function downloadCampaignStatistics($campaigns, $session) {
		foreach ($campaigns as $campaign) {
			$campaignIds[] = $campaign['id'];
			
			$campaignCreationTimestamp = $this->sklikCampaigns
					->getCampaignCreationTimestamp($campaign['id']);
			$dateFrom = SklikXML::createDate($campaignCreationTimestamp);
			$methodParams = $this->sklikStatisticsGeneral
					->getDefaultParams($dateFrom, $session);
			
			$statistics = $this->sklikCampaignStatistics
					->getStatistics($campaignIds, $methodParams);
			if ($statistics) {
				$this->sklikCampaignStatistics
						->insertStatisticsIntoDb($statistics);	
			}
		}
	}
	
	private function downloadGroupStatistics($groups, $session) {
		foreach ($groups as $group) {
			// tento parametr pro sklik xml-rpc metodu je vzdy pole
			$groupIds[] = $group['id'];
			
			$groupCreationTimestamp = $this->sklikGroups
					->getGroupCreationTimestamp($group['id']);
			
			$dateFrom = SklikXML::createDate($groupCreationTimestamp);
			$methodParams = $this->sklikStatisticsGeneral
					->getDefaultParams($dateFrom, $session);
			
			$statistics = $this->sklikGroupStatistics
				->getStatistics($groupIds, $methodParams);
			if ($statistics) {
				$this->sklikGroupStatistics->insertStatisticsIntoDb($statistics);
			}
		}
	}
	
	private function downloadKeywordStatistics($keywords, $session) {
		foreach ($keywords as $keyword) {
			// tento parametr pro sklik xml-rpc metodu je vzdy pole
			$keywordIds[] = $keyword['id'];
			
			$groupCreationTimestamp = $this->sklikKeywords
					->getGroupCreationTimestamp($keyword['id']);
			
			$dateFrom = SklikXML::createDate($groupCreationTimestamp);
			$methodParams = $this->sklikStatisticsGeneral
					->getDefaultParams($dateFrom, $session);
			
			$statistics = $this->sklikKeywordStatistics
				->getStatistics($keywordIds, $methodParams);
			if ($statistics) {
				$this->sklikKeywordStatistics->insertStatisticsIntoDb($statistics);
			}
		}
	}
    
    public function downloadSklikData($userId) {
        $sklikAccountPk = $this->userManager->getUsersActivatedSklikAccountPk($userId);
		$session = $this->sklikAdAccounts->getAccountSession($sklikAccountPk);
        
		$this->userManager->renewUsersActivatedSklikSession($userId);
		
		$marketingTree = $this->downloadMarketingTree($sklikAccountPk, $session);
		
		$campaigns = $marketingTree['campaigns'];
		$this->downloadCampaignStatistics($campaigns, $session);
		
		$groups = $marketingTree['groups'];
		$this->downloadGroupStatistics($groups, $session);
		
		$groupsKeywords = $marketingTree['keywords'];
		foreach ($groupsKeywords as $keywords) {
			$this->downloadKeywordStatistics($keywords, $session);
		}
    }
}