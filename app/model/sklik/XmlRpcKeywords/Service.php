<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model\Sklik\XmlRpcKeywords;
/**
 * Description of Services
 *
 * @author dave
 */
class Service {
	const
		CLIENT = 'client',
		GROUPS = 'groups',
		KEYWORDS = 'keywords',
		CAMPAIGNS = 'campaigns';
}
