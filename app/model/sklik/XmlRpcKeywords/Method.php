<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model\Sklik\XmlRpcKeywords;
/**
 * Description of Methods
 *
 * @author dave
 */
class Method {
	const
		STATS = 'stats',
		LOGIN = 'login',
		LIST_METHOD = 'list',
		GET = 'get';
}
