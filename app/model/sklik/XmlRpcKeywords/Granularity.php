<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik\XmlRpcKeywords;

/**
 * Description of Granularity
 *
 * @author dave
 */
class Granularity {
	const
	/* Granularity options */
		TOTAL = 'total',
		DAILY = 'daily',
		WEEKLY = 'weekly',
		MONTHLY = 'monthly',
		QUARTERLY = 'quarterly',
		YEARLY = 'yearly';
}
