<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model\Sklik\XmlRpcKeywords;

/**
 * Description of Attributes
 *
 * @author dave
 */
class Attribute {
	const
		ID = 'id',
		USER = 'user',
		USERNAME = 'username',
		USER_ID = 'userId',
		PARAMS = 'params',
		NAME = 'name',
		DELETED = 'deleted',
		CPC = 'cpc',
		CPC_CONTEXT = 'cpc',
		CPT = 'cpt',
		CREATE_DATE = 'createDate',
		GROUPS = 'groups',
		GROUP = 'group',
		KEYWORDS = 'keywords',
		KEYWORD_ID = 'keywordId',
		DISABLED = 'disabled',
		URL = 'url',
		MATCH_TYPE = 'matchType',
		CAMPAIGN = 'campaign',
		CAMPAIGN_IDS = 'campaignIds',
		MAX_USER_DAILY_IMPRESSION = 'maxUserDailyImpression',
		SESSION = 'session',
		SESSION_STARTED = 'sessionStarted',
		REPORT = 'report',
		DATE = 'date',
		DATE_FROM = 'dateFrom',
		DATE_TO = 'dateTo',
		GRANULARITY = 'granularity',
		STATS = 'stats',
		DATETIME = 'datetime',
		TIMESTAMP = 'timestamp',
		XMLRPC_TYPE = 'xmlrpc_type',
		SCALAR = 'scalar',
		STATUS = 'status',
		STATUS_MESSAGE = 'statusMessage',
		GROUP_IDS = 'groupIds',
		CAMPAINGS = 'campaigns',
		CLICKS = 'clicks',
		IMPRESSIONS = 'impressions',
		CTR = 'ctr',
		PRICE = 'price',
		AVG_POSITION = 'avgPosition',
		CONVERSIONS = 'conversions',
		CONVERSION_RATIO = 'conversionRatio',
		CONVERSION_AVG_PRICE = 'conversionAvgPrice',
		CONVERSION_VALUE = 'conversionValue',
		CONVERSION_AVG_VALUE = 'conversionAvgValue',
		CONVERSION_VALUE_RATIO = 'conversionValueRatio',
		TRANSACTIONS = 'transactions',
		TRANSACTION_AVG_PRICE = 'transactionAvgPrice',
		TRANSACTION_AVG_VALUE = 'transactionAvgValue',
		TRANSACTION_AVG_COUNT = 'transactionAvgCount',
		GROUP_ID = 'groupId',
		KEYWORDS_IDS = 'keywordIds';
}
