<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sklikAuthorization
 *
 * @author davidkosvica
 */

namespace App\Model\Sklik;
use Nette;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\XmlRpcKeywords\Method;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\DbInterface\Sklik\SklikAccountTable;

class SklikAdAccounts {
    /** @var SklikXML  */
    private $sklikHelperFunctions;
    
    /** @var SklikAccountTable  */
    private $sklikAccountTable;
    
    public function __construct(Nette\Database\Context $database) {
        $this->sklikHelperFunctions = new SklikXML();
        $this->sklikAccountTable = new SklikAccountTable($database);
    }
	
	public function clientLogin($email, $password) {
		$service = Service::CLIENT;
		$method = $service . '.' . Method::LOGIN;
		$params = [$email, $password];
		$response = $this->sklikHelperFunctions->executeMethod($method, $params);
        $session = $response[Attribute::SESSION];
        $clientAttributes = $this->clientGet($session);
		
        return $clientAttributes;
	}
    
    public function insertAdAccount($accountAttributes) {
        return $this->sklikAccountTable->insert($accountAttributes);
    }
    
    protected function clientGetParseResponse($response) {
        $newSession = $response[Attribute::SESSION];
        $sessionStarted = $response[Attribute::SESSION_STARTED];
        $sessionTimestamp = $sessionStarted->{Attribute::TIMESTAMP};
        $user = $response[Attribute::USER];
        $username = $user[Attribute::USERNAME];
        $userId = $user[Attribute::USER_ID];
        
        $clientAttributes = [
            'session' => $newSession,
            'session_timestamp' => $sessionTimestamp,
            'username' => $username,
            'userId'=> $userId
        ];
        
        return $clientAttributes;
    }
    
    public function setNewSession(string $session, int $sklikAccountPk) {
        $affected = $this->sklikAccountTable->setSession($session, $sklikAccountPk);
        
        if ($affected) {
            $success = true;
        } else {
            $success = false;
        }
        
        return $success;
    }
    
    public function getAccountSession(int $sklikAccountPk) {
        return $this->sklikAccountTable->getSession($sklikAccountPk);
    }
    
    public function clientGet($session) {
        $params = [Attribute::SESSION => $session ];
		$service = Service::CLIENT;
		$method = $service . '.' . Method::GET;
		$response = $this->sklikHelperFunctions->executeMethod($method, $params);
        $clientAttributes = false;
        
		$session = $response[Attribute::SESSION];
		$clientAttributes = $this->clientGetParseResponse($response);
		
        return $clientAttributes;
    }
    
	public function sklikVersion() {
		$request = xmlrpc_encode_request("api.version", []);
		$context = $this->sklikHelperFunctions->getContext($request);
		$file = file_get_contents(SklikXML::SKLIK_ENDPOINT, false, $context);
		$response = xmlrpc_decode($file);
		
		$this->sklikHelperFunctions->checkResponse($response);
        \Tracy\Debugger::barDump($response);
	}
	
	public function getSklikUsrIdBasedOnPk($primaryKey) {
		return $this->sklikAccountTable->getSklikUsrId($primaryKey);
	}
}
