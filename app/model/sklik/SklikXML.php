<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\Sklik\BadSessionException;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\BadArgumentsException;
use DateTime;
use Nette;

/**
 * Description of sklikXmlRpc
 *
 * @author david
 */
class SklikXML {
	
    const
		// sklik enpoint for making xml-rpc 
		SKLIK_ENDPOINT = 'https://api.sklik.cz/RPC2',
		BAD_SESSION_MSG = 'Session has expired or is malformed.',
		BAD_ARGUMENTS_MSG = 'Bad arguments',
		FAULT_STRING = 'faultString',
		FAULT_CODE = 'faultCode',
		OK_MESSAGE = 'OK';
		
	static function createDate(string $time) {
		$date = date(Datetime::ISO8601, $time);
		$timestamp = $time;
		$type = Attribute::DATETIME;
		
		$datetime = [Attribute::SCALAR => $date, 
			Attribute::XMLRPC_TYPE => $type, 
			Attribute::TIMESTAMP => $timestamp];
		
		return (object)$datetime;
	}
    
	private function getContext($request) {
        $context = stream_context_create(array('http' => array(
			'method' => "POST",
			'header' => "Content-Type: text/xml",
			'content' => $request
		)));
        return $context;
    }
    
    public function executeMethod($method, array $params) {
        $request = xmlrpc_encode_request($method, $params);
        $context = $this->getContext($request);
		$file = file_get_contents(self::SKLIK_ENDPOINT, false, $context);
		$response = xmlrpc_decode($file);
		$this->checkResponse($response);
        return $response;
    }
	
	protected function isOk($response) {
		$equals200 = $response && ($response[Attribute::STATUS] == 200);
		$isOk = $response[Attribute::STATUS_MESSAGE] == self::OK_MESSAGE;
		return $equals200 && $isOk;
	}
	
	protected function isBadSession($response) {
		$equals401 = $response && ($response[Attribute::STATUS] == 401);
		$isBadSession = $response[Attribute::STATUS_MESSAGE] == self::BAD_SESSION_MSG;
		return $equals401 && $isBadSession;
	}
	
	protected function isFault($response) {
		return $response && xmlrpc_is_fault($response);
	}
	
	protected function hasBadArguments($response) {
		$equals400 = $response[Attribute::STATUS] == 400;
		$isBadArg = $response[Attribute::STATUS_MESSAGE] == self::BAD_ARGUMENTS_MSG;
		return $response && $equals400 && $isBadArg;
	}
    
    private function checkResponse($response) {
//		\Tracy\Debugger::$maxDepth = 10;
//		\Tracy\Debugger::barDump($response);
		if ($this->isFault($response)) {
            throw new \Exception("xmlrpc: {$response[self::FAULT_STRING]} ({$response[self::FAULT_CODE]})");
		} else if ($this->isOk($response)) {
            return;
        } else if ($this->isBadSession($response)) {
            throw new BadSessionException();
		} else if ($this->hasBadArguments($response)) {
			throw new BadArgumentsException();
		} else {
            throw new Nette\Security\AuthenticationException();
		}
    }
}
