<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\XmlRpcKeywords\Method;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\DbInterface\Sklik\SklikKeywordTable;
use App\Model\DbInterface\Sklik\SklikGroupTable;
use Nette\Database\Context;
/**
 * Description of SklikKeywords
 *
 * @author dave
 */
class SklikKeywords {
	
	/** @var SklikXML  */
    private $sklikHelperMethods;
	
	/** @var SklikKeywordTable  */
    private $sklikKeywordsTable;
	
	/** @var SklikGroupTable  */
    private $sklikGroupTable;
	
	public function __construct(Context $database) {
        $this->sklikHelperMethods = new SklikXML();
        $this->sklikKeywordsTable = new SklikKeywordTable($database);
        $this->sklikGroupTable = new SklikGroupTable($database);
    }
	
	private function getKeywordsParseResponse($response) {
        $newSeesion = $response[Attribute::SESSION];
		$keywords = $response[Attribute::KEYWORDS];
		$parsedKeywords = [];
		foreach ($keywords as $keyword) {
			$groupId = $keyword[Attribute::GROUP][Attribute::ID];
			$parsedKeywords[$groupId][] = [
				'id' => $keyword[Attribute::ID],
				'name' => $keyword[Attribute::NAME],
				'status' => $keyword[Attribute::STATUS],
				'group' => $keyword[Attribute::GROUP],
				'deleted' => $keyword[Attribute::DELETED],
				'cpc' => $keyword[Attribute::CPC],
				'disabled' => $keyword[Attribute::DISABLED],
				'url' => $keyword[Attribute::URL],
				'matchType' => $keyword[Attribute::MATCH_TYPE],
				'createDate' => $keyword[Attribute::CREATE_DATE],
			];
		}
		$parsedResponse = ['newSession' => $newSeesion, 'keywords' => $parsedKeywords];
		return $parsedResponse;
	}
	
	public function getKeywords(string $session, array $groupIds) {
		$user = [
			'session' => $session,
		];
		
		$restrictionFilter = [
			'groupIds' => $groupIds
		];
		
		$method = Service::KEYWORDS . '.' . Method::LIST_METHOD;
		$params = [$user, $restrictionFilter];
		$response = $this->sklikHelperMethods->executeMethod($method, $params);
		$parsedResponse = $this->getKeywordsParseResponse($response);
		
		return $parsedResponse;
	}
	
//	public function insertKeywordsIntoDb($groupsKeywords) {
//		foreach ($groupsKeywords as $groupId => $keywords) {
//			foreach ($keywords as $keyword) {
//				$this->putKeywordIntoDb($keyword, $groupId);
//			}
//		}
//	}
	
	
	public function insertKeywordsIntoDb($groupsKeywords) {
		foreach ($groupsKeywords as $groupId => $keywords) {
			$this->putKeywordsIntoDb($keywords, $groupId);
		}
	}
	
//	private function putKeywordIntoDb($keyword, $groupId) {
//		$groupPk = $this->sklikGroupTable->getPk($groupId);
//		$this->sklikKeywordsTable->insert($keyword, $groupPk);
//	}
	
	private function putKeywordsIntoDb($keywords, $groupId) {
		$groupPk = $this->sklikGroupTable->getPk($groupId);
		$this->sklikKeywordsTable->insert($keywords, $groupPk);
	}
	
	public function getGroupCreationTimestamp($keywordId) {
		$dateTimeStr = $this->sklikKeywordsTable->getCreationDate($keywordId);
		$dateTimeObj = new \DateTime($dateTimeStr);
		return $dateTimeObj->getTimestamp();
	}
}
