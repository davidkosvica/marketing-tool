<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\XmlRpcKeywords\Method;
use App\Model\DbInterface\Sklik\SklikCampaignTable;
use Nette;

/**
 * Description of SklikAdCampaigns
 *
 * @author david
 */
class SklikCampaigns {
    
    /** @var SklikXML  */
    private $sklikHelperFunctions;
    
	/** @var SklikCampaignTable  */
    private $sklikCampaignTable;
	
    public function __construct(Nette\Database\Context $database) {
        $this->sklikHelperFunctions = new SklikXML();
		$this->sklikCampaignTable = new SklikCampaignTable($database);
    }
	
	private function getCampaignsParseResponse(array $response) {
		$campaigns = $response[Attribute::CAMPAINGS];
		$newSession = $response[Attribute::SESSION];
		return [
			'campaigns' => $campaigns,
			'newSession' => $newSession
		];
	}

	public function getCampaigns(string $session, int $userId = null) {
		$user = [
			Attribute::SESSION => $session,
        ];
		
		if ($userId) {
			$user[Attribute::USER_ID] = $userId;
		}
		
		$service = Service::CAMPAIGNS;
		$method = $service . '.' . Method::LIST_METHOD;
		
        $response = $this->sklikHelperFunctions->executeMethod($method, $user);
		
		$response = $this->getCampaignsParseResponse($response);
		
		return $response;
    }
	
	public function insertCampaignIntoDb($campaign_attributes, $sklikAccountPk) {
		$this->sklikCampaignTable->insert($campaign_attributes, $sklikAccountPk);
		
    }
    
    public function getCampaignPk($idFromSklik) {
        return $this->sklikCampaignTable->getPk($idFromSklik);
    }
	
	public function getCampaignCreationTimestamp($campaignId) {
		$dateTimeStr = $this->sklikCampaignTable->getCreationDate($campaignId);
		$dateTimeObj = new \DateTime($dateTimeStr);
		return $dateTimeObj->getTimestamp();
	}
	
}
