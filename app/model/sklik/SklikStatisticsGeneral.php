<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\XmlRpcKeywords\Granularity;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\XmlRpcKeywords\Method;
use App\Model\Sklik\XmlRpcKeywords\Service;

/**
 * Description of SklikStatisticsGeneral
 *
 * @author dave
 */
class SklikStatisticsGeneral {
	
	/** @var SklikXML  */
    private $sklikXMLHelperMethods;
    
	public function __construct() {
        $this->sklikXMLHelperMethods = new SklikXML();
    }
	
	public function parseResponse($response) {
		$newSession = $response[Attribute::SESSION];
		$reports = $response[Attribute::REPORT];
		$parsedResponse = [
			'newSession' => $newSession,
			'reports' => $reports,
		];

		return $parsedResponse;
	}
	
	public function getDefaultParams($dateFrom, $session) {
		$user = [
			Attribute::SESSION => $session,
		];
		
		$dateTo = SklikXML::createDate(time());
		$params[Attribute::DATE_FROM] = $dateFrom;
		$params[Attribute::DATE_TO] = $dateTo;
		$params[Attribute::GRANULARITY] = Granularity::DAILY;
		
		$methodParams[Attribute::USER] = $user;
		$methodParams[Attribute::PARAMS] = $params;
		
		return $methodParams;
	}
	
	public function getStatistics(array $params, string $service) {
		$method = $service . '.' . Method::STATS;
		$response = $this->sklikXMLHelperMethods
				->executeMethod($method, $params);
		
		return $response;
	}
	
	public function howMuchToDownload($creationDate, $numOfRecords) {
		$now = time();
		$creationDate = strtotime($creationDate);
		$datediff = $now - $creationDate;
		$secondsInDay = 60 * 60 * 24;
		$numDaysDiff = $datediff / $secondsInDay;
		
		return ($numDaysDiff-$numOfRecords) * $secondsInDay;
	}
}