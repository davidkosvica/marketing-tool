<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\DbInterface\Sklik\SklikCampaignStatsTable;
use App\Model\DbInterface\Sklik\SklikCampaignTable;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\SklikStatisticsGeneral;
use Nette\Database\Context;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\XmlRpcKeywords\Method;

/**
 * Description of SklikCampaignStatistics
 *
 * @author dave
 */
class SklikCampaignStatistics {
	/** @var SklikXML  */
    private $sklikXMLHelperMethods;
    
    /** @var SklikCampaignStatsTable  */
    private $sklikCampaignStatsTable;
	
	/** @var SklikCampaignTable $sklikCampaigns   */
    private $sklikCampaignTable;
	
	/** @var SklikStatisticsGeneral $sklikStatisticsGeneral */
    private $sklikStatisticsGeneral;
    
	public function __construct(Context $database) {
        $this->sklikXMLHelperMethods = new SklikXML();
        $this->sklikCampaignStatsTable = new SklikCampaignStatsTable($database);
        $this->sklikCampaignTable = new SklikCampaignTable($database);
        $this->sklikStatisticsGeneral = new SklikStatisticsGeneral($database);
    }
	
	private function campaignStatsParse($response) {
		$parsedResponse = $this->sklikStatisticsGeneral
				->parseResponse($response);
		$reports = $parsedResponse['reports'];
		foreach($reports as $report) {
			$parsedReports[$report['campaignId']] = $report['stats'];
		}
		
		$parsedResponse['reports'] = $parsedReports;
		return $parsedResponse;
	}
	
	public function getStatistics(array $campaignIds, array $methodParams) {
		$service = Service::CAMPAIGNS;
	
		// pravdepoodbne nepotrebne:
//		$numOfRecords = $this->sklikCampaignStatsTable->getNumRows();
//		$creationDate = $methodParams[Attribute::PARAMS][Attribute::DATE_FROM]
//				->{Attribute::SCALAR};
//		$howMuchToDownload = $this->sklikStatisticsGeneral
//				->howMuchToDownload($creationDate, $numOfRecords);
//		$dateFrom = SklikXML::createDate(time() - $howMuchToDownload);
//		$methodParams[Attribute::PARAMS][Attribute::DATE_FROM] = $dateFrom; 
		
		$methodParams[Attribute::CAMPAIGN_IDS] = $campaignIds;
		$formattedParmas = [
			$methodParams[Attribute::USER], 
			$methodParams[Attribute::CAMPAIGN_IDS], 
			$methodParams[Attribute::PARAMS]
		];
		
		$response = $this->sklikStatisticsGeneral
				->getStatistics($formattedParmas, $service);
		if (!empty($response[Attribute::REPORT])) {
			$parsedResponse = $this->campaignStatsParse($response);
		} else {
			$parsedResponse = false;
		}
		
		return $parsedResponse;
	}
	
//	private function insertReports($reports, $campaignPk) {
//		foreach ($reports as $report) {
//			$this->sklikCampaignStatsTable->insert($report, $campaignPk);
//		}
//	}
	
	public function insertStatisticsIntoDb($statistics) {
		$campaignsReports = $statistics['reports'];

		foreach ($campaignsReports as $campaignId => $reports) {
			$campaignPk = $this->sklikCampaignTable->getPk($campaignId);
			$this->sklikCampaignStatsTable->insert($reports, $campaignPk);
		}
	}
}
