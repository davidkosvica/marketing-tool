<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\XmlRpcKeywords\Method;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\SklikXML;
use App\Model\DbInterface\Sklik\SklikGroupTable;
use App\Model\DbInterface\Sklik\SklikGroupToCampaignTable;
use App\Model\DbInterface\Sklik\SklikCampaignTable;
use Nette;
/**
 * Description of SklikGroups
 *
 * @author david
 */
class SklikGroups {

	/** @var SklikXML  */
    private $sklikHelperMethods;
    
    /** @var SklikGroupTable  */
    private $sklikGroupTable;
    
    /** @var SklikGroupToCampaignTable  */
    private $sklikGroupToCampaignTable;
    
	/** @var SklikCampaignTable  */
    private $sklikCampaignTable;
	
	
	
	public function __construct(Nette\Database\Context $database) {
        $this->sklikHelperMethods = new SklikXML();
        $this->sklikGroupTable = new SklikGroupTable($database);
        $this->sklikGroupToCampaignTable = new SklikGroupToCampaignTable($database);
        $this->sklikCampaignTable = new SklikCampaignTable($database);
    }
	
	private function getGroupsParseResponse($response) {
        $newSeesion = $response[Attribute::SESSION];
		$groups = $response[Attribute::GROUPS];
		$parsedGroups = [];
		foreach ($groups as $group) {
			$campaignId = $group[Attribute::CAMPAIGN][Attribute::ID];
			$parsedGroups[$campaignId] = [
				'id' => $group[Attribute::ID],
				'name' => $group[Attribute::NAME],
				'status' => $group[Attribute::STATUS],
				'deleted' => $group[Attribute::DELETED],
				'cpc' => $group[Attribute::CPC],
				'cpcContext' => $group[Attribute::CPC_CONTEXT],
				'cpt' => $group[Attribute::CPT],
				'createDate' => $group[Attribute::CREATE_DATE],
				'maxUserDailyImpression' => $group[Attribute::MAX_USER_DAILY_IMPRESSION],
			];
		}
		
		return [ 'newSession' => $newSeesion, 'groups' => $parsedGroups];
	}
	
	public function getGroups(string $session, array $campaignIds) {
		// musi byt pole, i kdyz jen jednoprvkove
        $service = Service::GROUPS;
		$method = Method::LIST_METHOD;
		$userAttr = [Attribute::SESSION => $session];
		$filterAttr = [Attribute::CAMPAIGN_IDS => $campaignIds];
		$params = [ $userAttr, $filterAttr ];
		
		$response = $this->sklikHelperMethods->executeMethod($service . '.' . $method, $params);
		$parsedResponse = $this->getGroupsParseResponse($response);
        
		return $parsedResponse;
	}
    
    private function putGroupIntoDb(array $group, $campaignPk) {
		$groupPk = $this->sklikGroupTable->insert($group, $campaignPk);
		if ($groupPk) {
			$this->sklikGroupToCampaignTable->insert($groupPk, $campaignPk);
		}
	}
	
	public function insertGroupsIntoDb($groups) {
		foreach($groups as $campaignId => $group) {
			$campaignPk = $this->sklikCampaignTable->getPk($campaignId);
			$this->putGroupIntoDb($group, $campaignPk);
		}
	}
	
	public function getGroupCreationTimestamp($groupId) {
		$dateTimeStr = $this->sklikGroupTable->getCreationDate($groupId);
		$dateTimeObj = new \DateTime($dateTimeStr);
		return $dateTimeObj->getTimestamp();
	}
	
}
