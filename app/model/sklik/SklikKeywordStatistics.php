<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\DbInterface\Sklik\SklikKeywordTable;
use App\Model\DbInterface\Sklik\SklikKeywordStatsTable;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\SklikStatisticsGeneral;
use Nette\Database\Context;


/**
 * Description of SklikKeywordsStatistics
 *
 * @author dave
 */
class SklikKeywordStatistics {
	/** @var SklikXML  */
    private $sklikXMLHelperMethods;
    
    /** @var SklikKeywordStatsTable  */
    private $sklikKeywordStatsTable;
    
	/** @var SklikKeywordTable  */
    private $sklikKeywordTable;
	
	/** @var SklikStatisticsGeneral $sklikStatisticsGeneral */
    private $sklikStatisticsGeneral;
    
	public function __construct(Context $database) {
        $this->sklikXMLHelperMethods = new SklikXML();
        $this->sklikKeywordStatsTable = new SklikKeywordStatsTable($database);
        $this->sklikKeywordTable = new SklikKeywordTable($database);
		$this->sklikStatisticsGeneral = new SklikStatisticsGeneral($database);
    }
	
	private function keywordParseStats($response) {
		$newSession = $response[Attribute::SESSION];
		$reports = $response[Attribute::REPORT];
		foreach ($reports as $report) {
			$parsedReports[$report[Attribute::KEYWORD_ID]] = 
					$report[Attribute::STATS];
		}
		$parsedResponse = [Attribute::REPORT => $parsedReports];
		return $parsedResponse;
	}
	
	public function getStatistics(array $keywordIds, array $methodParams) {
		$service = Service::KEYWORDS;
		
		// pravdepoodbne nepotrebne:
//		$numOfRecords = $this->sklikGroupStatsTable->getNumRows();
//		$creationDate = $methodParams[Attribute::PARAMS][Attribute::DATE_FROM]
//				->{Attribute::SCALAR};
//		$howMuchToDownload = $this->sklikStatisticsGeneral
//				->howMuchToDownload($creationDate, $numOfRecords);
//		$dateFrom = SklikXML::createDate(time() - $howMuchToDownload);
//		$methodParams[Attribute::PARAMS][Attribute::DATE_FROM] = $dateFrom; 

		$methodParams[Attribute::KEYWORDS_IDS] = $keywordIds;
		$formattedParams = [
			$methodParams[Attribute::USER],
			$methodParams[Attribute::KEYWORDS_IDS],
			$methodParams[Attribute::PARAMS],
		];
		
		$response = $this->sklikStatisticsGeneral
				->getStatistics($formattedParams, $service);
		
		if (!empty($response[Attribute::REPORT])) {
			$parsedResponse = $this->keywordParseStats($response);
		} else {
			$parsedResponse = false;
		}
		
		return $parsedResponse;
	}
	
	public function insertStatisticsIntoDb($statistics) {
		$keywordReports = $statistics[Attribute::REPORT];
		foreach ($keywordReports as $keywordId => $reports) {
			$keywordPk = $this->sklikKeywordTable->getPk($keywordId);
			$this->sklikKeywordStatsTable->insert($reports, $keywordPk);
		}
	}
}
