<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Sklik;
use App\Model\DbInterface\Sklik\SklikGroupTable;
use App\Model\Sklik\XmlRpcKeywords\Service;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\SklikStatisticsGeneral;
use App\Model\DbInterface\Sklik\SklikGroupStatsTable;
use Nette\Database\Context;

/**
 * Description of SklikGroupsStatistics
 *
 * @author dave
 */
class SklikGroupStatistics {
	
	/** @var SklikXML  */
    private $sklikXMLHelperMethods;
    
    /** @var SklikGroupStatsTable  */
    private $sklikGroupStatsTable;
    
	/** @var SklikGroupTable  */
    private $sklikGroupTable;
	
	/** @var SklikStatisticsGeneral $sklikStatisticsGeneral */
    private $sklikStatisticsGeneral;
    
	public function __construct(Context $database) {
        $this->sklikXMLHelperMethods = new SklikXML();
        $this->sklikGroupStatsTable = new SklikGroupStatsTable($database);
        $this->sklikGroupTable = new SklikGroupTable($database);
		$this->sklikStatisticsGeneral = new SklikStatisticsGeneral($database);
    }
	
	private function groupParseStats($response) {
		$newSession = $response[Attribute::SESSION];
		$reports = $response[Attribute::REPORT];
		foreach ($reports as $report) {
			$parsedReports[$report[Attribute::GROUP_ID]] = 
					$report[Attribute::STATS];
		}
		$parsedResponse = [Attribute::REPORT => $parsedReports];
		return $parsedResponse;
	}
	
	public function getStatistics(array $groupIds, array $methodParams) {
		$service = Service::GROUPS;
		
		// pravdepoodbne nepotrebne:
//		$numOfRecords = $this->sklikGroupStatsTable->getNumRows();
//		$creationDate = $methodParams[Attribute::PARAMS][Attribute::DATE_FROM]
//				->{Attribute::SCALAR};
//		$howMuchToDownload = $this->sklikStatisticsGeneral
//				->howMuchToDownload($creationDate, $numOfRecords);
//		$dateFrom = SklikXML::createDate(time() - $howMuchToDownload);
//		$methodParams[Attribute::PARAMS][Attribute::DATE_FROM] = $dateFrom; 

		$methodParams[Attribute::GROUP_IDS] = $groupIds;
		$formattedParams = [
			$methodParams[Attribute::USER],
			$methodParams[Attribute::GROUP_IDS],
			$methodParams[Attribute::PARAMS],
		];
		
		$response = $this->sklikStatisticsGeneral
				->getStatistics($formattedParams, $service);
		
		if (!empty($response[Attribute::REPORT])) {
			$parsedResponse = $this->groupParseStats($response);
		} else {
			$parsedResponse = false;
		}
		
		return $parsedResponse;
	}
	
	public function insertStatisticsIntoDb($statistics) {
		$groupReports = $statistics[Attribute::REPORT];
		

		foreach ($groupReports as $groupId => $reports) {
			$groupPk = $this->sklikGroupTable->getPk($groupId);
			$this->sklikGroupStatsTable->insert($reports, $groupPk);
		}
	}
}
