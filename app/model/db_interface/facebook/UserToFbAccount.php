<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Facebook;

use Nette;
use App\Model\DbInterface\Facebook\FacebookAccountTable;
use App\Model\DbInterface\States;

/**
 * Description of UserToFbAccount
 *
 * @author davidkosvica
 */
class UserToFbAccount {
	const TABLE_NAME = 'user_to_fb_account';
	const USER_FK = 'id_user';
	const ACCOUNT_FB_FK = 'id_facebook_account';
	const STATE = 'state';
	
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var \App\Model\DbInterface\Facebook\FacebookAccountTable */
	private $facebookAccountTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->facebookAccountTable = new FacebookAccountTable($database);
	}
	
	public function insertUserFbAccountLink($userId, $fbAccountIdPk) {
		$row = $this->database->table(self::TABLE_NAME)->insert([
			self::USER_FK => $userId,
			self::ACCOUNT_FB_FK => $fbAccountIdPk,
			self::STATE => States::Inactive
		]);
		return $row;
	}
	
	public function getUsersFbAccount($userId) {
		/* @var $usrToFbAccLink Nette\Database\Table\ActiveRow */
		$usrToFbAccLink = $this->database
			->table(self::TABLE_NAME)
			->where(self::USER_FK, $userId)
			->where(self::STATE, States::Active)
			->fetch();
		
		if ($usrToFbAccLink) {
			$fbAccountId = $usrToFbAccLink
				->ref(FacebookAccountTable::TABLE_NAME, self::ACCOUNT_FB_FK)
				->{FacebookAccountTable::FACEBOOK_ID};
		} else {
			$fbAccountId = false;
		}
		return $fbAccountId;
	}
	
	public function getFbAccountByFbIdAndUserId($fbAccountId, $userId) {
		$fbAccount = $this->database
			->table(self::TABLE_NAME)
			->where(self::USER_FK, $userId)
			->where(self::ACCOUNT_FB_FK, $fbAccountId)
			->fetch();
		
		return $fbAccount;
	}
	
	public function enableFbAccount($userId, $fbAccountId) {
		$fbAccountPk = $this->facebookAccountTable->getFbAccountPkBaseOnFbId($fbAccountId);
		
		$this->database
				->table(self::TABLE_NAME)
				->where(self::ACCOUNT_FB_FK, $fbAccountPk)
				->where(self::USER_FK, $userId)
				->update([self::STATE => States::Active]);
       
	}
	
	public function disableFbAccount($userId, $fbAccountId) {
		$fbAccountPk = $this->facebookAccountTable->getFbAccountPkBaseOnFbId($fbAccountId);
		
		$this->database->table(self::TABLE_NAME)
				->where(self::ACCOUNT_FB_FK, $fbAccountPk)
				->where(self::USER_FK, $userId)
				->update([self::STATE => States::Inactive]);
	}
	
	public function test($userId, $fbAccountId) {
//		$fbAccountPk = $this->facebookAccountTable->getFbAccountPkBaseOnFbId($fbAccountId);
//		
//		$this->database
//				->table(self::TABLE_NAME)
//				->where(self::ACCOUNT_FB_FK, $fbAccountPk)
//				->where(self::USER_FK, $userId)
//		
//		$activeRow = $this->database
//				->table(self::TABLE_NAME)
//				->where(self::USER_FK, $userId)
//				->fetch();
//        $table = $this->database
//				->table(self::TABLE_NAME);
//		\Tracy\Debugger::barDump($table);
//
//		$result2 = $this->database
//				->table(self::TABLE_NAME)
//				->
//         
//         \Tracy\Debugger::barDump($result2);
//		
//		$this->database
//				->table(\App\Model\DbInterface\Facebook\FacebookAccountTable::TABLE_NAME)
//				->where(\App\Model\DbInterface\Facebook\FacebookAccountTable::FACEBOOK_ID, $fbAccountId)
//				->select(\App\Model\DbInterface\Facebook\FacebookAccountTable::PRIMARY_KEY)
	}
}