<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Facebook;
use Nette;

/**
 * Description of FacebookAccessTable
 *
 * @author davidkosvica
 */
class FacebookAccountTable {
	
	const TABLE_NAME = 'facebook_account';
	const FACEBOOK_ID = 'id_from_facebook';
	const ACCOUNT_NAME = 'name';
	const PRIMARY_KEY = 'id_facebook_account';
	const EMAIL = 'email';
	const IS_ACTIVE = 'active';
	const ACCESS_TOKEN = 'access_token';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function getFbAccountPkBaseOnFbId($fbAccountId) {
		$result = $this->database->table(self::TABLE_NAME)
				->where(self::FACEBOOK_ID, $fbAccountId)
				->select(self::PRIMARY_KEY)->fetch();
		
		if ($result) {
			return $result->{self::PRIMARY_KEY};
		} else {
			return null;
		}
	}
    
	public function getAccountPkBaseOnAccessToken($accessToken) {
		/* @var $result \Nette\Database\Table\ActiveRow */
		$result = $this->database->table(self::TABLE_NAME)
				->where(self::ACCESS_TOKEN, $accessToken)
				->select(self::PRIMARY_KEY)->fetch();
		
		
		if ($result) {
			$result = $result->{self::PRIMARY_KEY};
		}
		
		return $result;
	}
	
	public function insertFacebookAccount($fbAccountId, $userObject) {
		try {
			$row = $this->database->table(self::TABLE_NAME)->insert([
				self::FACEBOOK_ID => $fbAccountId,
				self::EMAIL => $userObject['email'],
				self::ACCOUNT_NAME => $userObject['name']
			]);
			$fbAccountPrimaryKey = $row->{self::PRIMARY_KEY};
		} catch (\Nette\Database\UniqueConstraintViolationException $e) {
			$fbAccountPrimaryKey = false;
		}
		
		return $fbAccountPrimaryKey;
	}
	
	public function updateAccessToken($facebookId, $accessToken) {
		$this->database->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $facebookId)
			->update([self::ACCESS_TOKEN => $accessToken]);
	}
	
	public function getAccessToken($fbAccountId) {
		$row = $this->database
			->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $fbAccountId)
			->select(self::ACCESS_TOKEN)
			->fetch();
		if ($row) {
			$result = $row->{self::ACCESS_TOKEN};
		} else {
			$result = false;
		}
		return $result;
	}
	
	public function getFbAccountBaseOnFbId($fbAccountId) {
		$row = $this->database
			->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $fbAccountId)
			->select(self::PRIMARY_KEY . ", " . self::USER_FK)->fetch();
		return $row;
	}
}

