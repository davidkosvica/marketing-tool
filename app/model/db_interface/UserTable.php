<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface;
use Nette\Security\Passwords;
use Nette;

/**
 * Description of UserTable
 *
 * @author davidkosvica
 */
class UserTable {
	
	private $userRow;
	
	const
		TABLE_NAME = 'user',
		PRIMARY_KEY = 'id_user',
		LOGIN_NAME = 'login',
		PASSWORD_HASH = 'password',
		EMAIL = 'email',
		ROLE = 'role';
	
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function getUserBaseOnLogin($userName) {
		return $this->database->table(self::TABLE_NAME)
			->where(self::LOGIN_NAME, $userName)->fetch();
	}
	
	public function insertUser($username, $email, $password, $password_verify)
	{   
        if ($password == $password_verify) {
			try {
				$this->database->table(self::TABLE_NAME)->insert([
					self::LOGIN_NAME => $username,
					self::PASSWORD_HASH => Passwords::hash($password),
					self::EMAIL => $email ]);
			} catch (Nette\Database\UniqueConstraintViolationException $e) {
				throw new DuplicateNameException;
			}
        } else {
			throw new PasswordsDoNotMatchException("Password do not match.");
		}
	}
	
	public function getAllUserIds() {
		$rows = $this->database->table(self::TABLE_NAME)
				->select(self::PRIMARY_KEY)
				->fetchAll();
		$ids = [];
		foreach($rows as $row) {
			$ids[] = $row->{self::PRIMARY_KEY};
		}
		return $ids;
	}
}

class DuplicateNameException extends \Exception
{
}

class PasswordsDoNotMatchException extends \Exception
{
}