<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;

/**
 * Description of AdwordsAdGroupsTable
 *
 * @author davidkosvica
 */

class AdwordsAdGroupTable {
	
	const TABLE_NAME = 'adwords_ad_group';
	const PRIMARY_KEY = 'id_adwords_ad_group';
	const AD_GROUP_ID = 'ad_group_id';
	const AD_GROUP_NAME = 'name';
	const CAMPAIGN_FK = 'id_adwords_campaign';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function insertAdGroup($primaryKeyCampaign, $adGroupName, $adGroupId) {
		$this->database->table(self::TABLE_NAME)
			->insert([
				self::CAMPAIGN_FK => $primaryKeyCampaign,
				self::AD_GROUP_NAME => $adGroupName,
				self::AD_GROUP_ID => $adGroupId
			]);
	}
}
