<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;

/**
 * Description of AdwordsApiColumnNames
 *
 * @author david
 */
class AdwordsApiColumnNames {
	const 
		CAMPAIGN_ID = 'Campaign ID',
		AD_GROUP_ID = 'Ad group ID',
		DATE = 'Day',
		IMPRESSIONS = 'Impressions',
		CLICKS = 'Clicks',
		AVG_CPC = 'Avg. CPC',
		CTR = 'CTR',
		CONVERSIONS = 'Conversions',
		AVG_POSITION = 'Avg. position',
		KEYWORD_ID = 'Keyword ID',
		KEYWORD = 'Keyword',
		CONV_RATE = 'Conv. rate';
}
