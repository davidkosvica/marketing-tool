<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;
use Nette;
use App\Model\Adwords\AdwordsAuthorization;
use Google\AdsApi\AdWords\v201802\cm\CampaignService;
use Google\AdsApi\AdWords\v201802\cm\OrderBy;
use Google\AdsApi\AdWords\v201802\cm\Paging;
use Google\AdsApi\AdWords\v201802\cm\Selector;
use Google\AdsApi\AdWords\v201802\cm\SortOrder;
use App\Model\DbInterface\AdWords\AdwordsApiColumnNames;
use App\Model\DbInterface\AdWords\AdwordsAdGroupTable;
use Nette\Database\UniqueConstraintViolationException;

/**
 * Description of AdwordsKeywordTable
 *
 * @author david
 */
class AdwordsKeywordTable {
	const TABLE_NAME = 'adwords_keyword',
			PK = 'id_adwords_keyword',	
			ID = 'keyword_id',	
			NAME = 'keyword_name',
			FK = 'id_adwords_ad_group';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function insertKeyword($keywordValues) {
		$adGroupRow = $this->database->table(AdwordsAdGroupTable::TABLE_NAME)
			->select(AdwordsAdGroupTable::PRIMARY_KEY)
			->where(AdwordsAdGroupTable::AD_GROUP_ID, $keywordValues[AdwordsApiColumnNames::AD_GROUP_ID])
			->fetch();
		
		try {
			return $this->database
			->table(self::TABLE_NAME)
			->insert([
				self::ID => $keywordValues[AdwordsApiColumnNames::KEYWORD_ID],
				self::FK => $adGroupRow->{AdwordsAdGroupTable::PRIMARY_KEY},
				self::NAME => $keywordValues[AdwordsApiColumnNames::KEYWORD]
			])->{self::PK};
		} catch (UniqueConstraintViolationException $e) {}
		
	}
	
//	public function getPkBaseOnCampaignId($campaignId) {
//		$result =  $this->database->table(self::TABLE_NAME)
//				->where(self::CAMPAIGN_ID, $campaignId)
//				->select(self::PRIMARY_KEY)->fetch();
//		
//		if ($result) {
//			return $result->{self::PRIMARY_KEY};
//		} else {
//			return null;
//		}
//	}
}
