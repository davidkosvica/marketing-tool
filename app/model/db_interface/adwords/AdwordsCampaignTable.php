<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;

/**
 * Description of AdwordsCampaignTable
 *
 * @author davidkosvica
 */
class AdwordsCampaignTable {
	
	const TABLE_NAME = 'adwords_campaign';
	const CUSTOMER_ID_FK = 'id_adwords_account';
	const CAMPAIGN_ID = 'campaign_id';
	const PRIMARY_KEY = 'id_adwords_campaign';
	const CAMPAIGN_NAME = 'name';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function insertCampaign($idCampaign, $idAdwordsAccount, $campaign) {
		return $this->database
			->table(self::TABLE_NAME)
			->insert([
				self::CAMPAIGN_ID => $idCampaign,
				self::CUSTOMER_ID_FK => $idAdwordsAccount,
				self::CAMPAIGN_NAME => $campaign->getName()
			])->{self::PRIMARY_KEY};
	}
	
	public function getPkBaseOnCampaignId($campaignId) {
		$result =  $this->database->table(self::TABLE_NAME)
				->where(self::CAMPAIGN_ID, $campaignId)
				->select(self::PRIMARY_KEY)->fetch();
		
		if ($result) {
			return $result->{self::PRIMARY_KEY};
		} else {
			return null;
		}
	}
}
