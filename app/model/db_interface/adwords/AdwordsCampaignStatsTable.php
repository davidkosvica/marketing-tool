<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;
use App\Model\DbInterface\AdWords\AdwordsApiColumnNames;
use App\Model\DbInterface\AdWords\AdwordsCampaignTable;
use Nette\Database\UniqueConstraintViolationException;
use \Nette\Database\Context;

/**
 * Description of AdwordsCampaignTable
 *
 * @author davidkosvica
 */
class AdwordsCampaignStatsTable {
	
	const TABLE_NAME = 'adwords_campaign_stats',
		PK = 'id_adwords_campaign_stats',
		FK = 'id_adwords_campaign',
		DATE = 'date',
		IMPRESSIONS = 'impressions',
		CLICKS = 'clicks',
		AVG_CPC = 'avg_cpc',
		CTR = 'ctr',
		CONVERSIONS = 'conversions',
		AVG_POSITION = 'avg_position',
		CONV_RATE = 'conv_rate';

	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(Context $database) {
		$this->database = $database;
	}
	
	public function insertCampaignStats($statsValues) {
		$activeRowWithPk = $this->database->table(AdwordsCampaignTable::TABLE_NAME)
			->select(AdwordsCampaignTable::PRIMARY_KEY)
			->where(AdwordsCampaignTable::CAMPAIGN_ID, $statsValues[AdwordsApiColumnNames::CAMPAIGN_ID])
			->fetch();
		try {
			return $this->database
				->table(self::TABLE_NAME)
				->insert([
					self::FK => $activeRowWithPk->getPrimary(),
					self::AVG_CPC => $statsValues[AdwordsApiColumnNames::AVG_CPC],
					self::AVG_POSITION => $statsValues[AdwordsApiColumnNames::AVG_POSITION],
					self::CLICKS => $statsValues[AdwordsApiColumnNames::CLICKS],
					self::CONVERSIONS => $statsValues[AdwordsApiColumnNames::CONVERSIONS],
					self::CONV_RATE => $statsValues[AdwordsApiColumnNames::CONV_RATE],
					self::CTR => $statsValues[AdwordsApiColumnNames::CTR],
					self::DATE => $statsValues[AdwordsApiColumnNames::DATE],
					self::IMPRESSIONS => $statsValues[AdwordsApiColumnNames::IMPRESSIONS]
				])->getPrimary();
		} catch (UniqueConstraintViolationException $e) {}
	}
	
//	public function getPkBaseOnCampaignId($campaignId) {
//		$result =  $this->database->table(self::TABLE_NAME)
//				->where(self::CAMPAIGN_ID, $campaignId)
//				->select(self::PRIMARY_KEY)->fetch();
//		
//		if ($result) {
//			return $result->{self::PRIMARY_KEY};
//		} else {
//			return null;
//		}
//	}
}
