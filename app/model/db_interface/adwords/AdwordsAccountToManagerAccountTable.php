<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;

/**
 * Description of AdwordsAccountToManagerAccount
 *
 * @author davidkosvica
 */
class AdwordsAccountToManagerAccountTable {
	
	const TABLE_NAME = 'adwords_account_to_manager_account';
	const CUSTOMER_ACCOUNT_FK = 'id_adwords_account';
	const MANAGER_ACCOUNT_FK = 'id_adwords_manager_account';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	/**
	 * Insert relationship between manager account and customer account
	 * @param int $managerPk
	 * @param int $customerPk
	 */
	public function insertRelationship($managerPk, $customerPk) {
		$this->database->table(self::TABLE_NAME)
				->insert([
					self::CUSTOMER_ACCOUNT_FK => $customerPk,
					self::MANAGER_ACCOUNT_FK => $managerPk
				]);
	}
}
