<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\AdWords;

/**
 * Description of AdwordsAccount
 *
 * @author davidkosvica
 */
class AdwordsAccountTable {
	
	const TABLE_NAME = 'adwords_account';
	const CUSTOMER_ID = 'managed_customer_id';
	const PRIMARY_KEY = 'id_adwords_account';
	const ACCOUNT_NAME = 'name';
	const GOOGLE_ACCOUNT_FK = 'id_google_account';
	const IS_MANAGER_ACCOUNT = 'is_manager_account';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function getPkBaseOnCustomerId($customerId) {
		$result = $this->database
			->table(self::TABLE_NAME)
			->where(self::CUSTOMER_ID, $customerId)
			->select(self::PRIMARY_KEY)->fetch();
		
		if ($result) {
			return $result->{self::PRIMARY_KEY};
		} else {
			return null;
		}
	}
	
	public function insertAdwordAccount($adwordAccount) {
		$this->database->table(self::TABLE_NAME)
			->insert([
				self::CUSTOMER_ID => $adwordAccount->getCustomerId(),
				self::ACCOUNT_NAME => $adwordAccount->getName()
			]);
	}
	
	public function insertManagerAdwordAccount($managerAdwordAccount, 
		$idGoogleAccount) {
		$this->database->table(self::TABLE_NAME)
			->insert([
				self::CUSTOMER_ID => $managerAdwordAccount->getCustomerId(),
				self::ACCOUNT_NAME => $managerAdwordAccount->getName(),
				self::GOOGLE_ACCOUNT_FK => $idGoogleAccount,
				self::IS_MANAGER_ACCOUNT => 1
			]);
	}
	
	public function getAdwordsAccountsIds() {
		return $this->database->table(self::TABLE_NAME)
			->select(self::CUSTOMER_ID . ", " . self::IS_MANAGER_ACCOUNT)->fetchAll();
	}
}
