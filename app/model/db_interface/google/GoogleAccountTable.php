<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Google;
use App\Model\DbInterface\Google\GoogleRefreshTokenTable;
use App\Model\DbInterface\Google\GoogleAccessTokenTable;

/**
 * Description of GoogleAccountTable
 *
 * @author davidkosvica
 */
class GoogleAccountTable {
	
	const TABLE_NAME = 'google_account';
	const USER_FK = 'id_user';
	const PRIMARY_KEY = 'id_google_account';
	const GOOGLE_ID = 'id_from_google';
	const ACCOUNT_NAME = 'name';
	const EMAIL = 'email';
	const ROLE = 'role';
	const IS_ACTIVE = 'active';
	
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var \App\Model\DbInterface\Google\GoogleAccessTokenTable */
	private $googleAccessTokenTable;
	
	/** @var \App\Model\DbInterface\Google\GoogleRefreshTokenTable */
	private $googleRefreshTokenTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->googleAccessTokenTable = new GoogleAccessTokenTable($database);
		$this->googleRefreshTokenTable = new GoogleRefreshTokenTable($database);
	}
	
	public function insertGoogleAccount($userObject) {
		try {
			$row = $this->database->table(self::TABLE_NAME)->insert([
				self::GOOGLE_ID => $userObject['id'],
				self::ACCOUNT_NAME => $userObject['name'],
				self::EMAIL => $userObject['email'],
			]);
		} catch (\Nette\Database\UniqueConstraintViolationException $e) {
			$row = $this->database->table(self::TABLE_NAME)
					->where(self::GOOGLE_ID, $userObject['id'])
					->select(self::PRIMARY_KEY)
					->fetch();
		}
		
		return $row->{self::PRIMARY_KEY};
	}
	
	public function getGoogleAccountPkBasedOnGoogleId($idFromGoogle) {
		/* @var $googleAccountRow \Nette\Database\Table\ActiveRow  */
		$googleAccountRow = $this->database->table(self::TABLE_NAME)
			->where(self::GOOGLE_ID, $idFromGoogle)->fetch();
		if ($googleAccountRow) {
			$result = $googleAccountRow->{self::PRIMARY_KEY};
		} else {
			$result = false;
		}
		
		return $result;
	}
	
}

class GoogleAccountNotFound extends \Exception {}