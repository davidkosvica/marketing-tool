<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Google;
use App\Model\DbInterface\Google\GoogleAccountTable;
use App\Model\DbInterface\States;

/**
 * Description of UserToGoogleAccount
 *
 * @author david
 */
class UserToGoogleAccount {
	
	const TABLE_NAME = 'user_to_google_account';
	const USER_FK = 'id_user';
	const ACCOUNT_GOOGLE_FK = 'id_google_account';
	const STATE = 'state';
		
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var App\Model\DbInterface\Google\GoogleAccountTable */
	private $googleAccountTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->googleAccountTable = new GoogleAccountTable($database);
	}
	
	/**
	 * 
	 * @param int $userId
	 * @param int $googleAccountIdPk
	 * @return Nette\Database\Table\ActiveRow 
	 */
	public function insertUserGoogleAccountLink($userId, $googleAccountIdPk) {
		try {
			$row = $this->database->table(self::TABLE_NAME)->insert([
				self::USER_FK => $userId,
				self::ACCOUNT_GOOGLE_FK => $googleAccountIdPk,
				self::STATE => States::Inactive
			]);
		} catch (\Nette\Database\UniqueConstraintViolationException $e) {}
	}
	
	/**
	 * 
	 * @param int $userId
	 * @return boolean 
	 * @return int
	 */
	public function getUsersActivatedGoogleAccountPk($userId) {
		/* @var $usrToFbAccLink Nette\Database\Table\ActiveRow */
		$usrToGoogleAccLink = $this->database
			->table(self::TABLE_NAME)
			->where(self::USER_FK, $userId)
			->where(self::STATE, States::Active)
			->select(self::ACCOUNT_GOOGLE_FK)
			->fetch();
		
		if ($usrToGoogleAccLink) {
			$googleAccountPk = $usrToGoogleAccLink->{self::ACCOUNT_GOOGLE_FK};
		} else {
			$googleAccountPk = false;
		}
		return $googleAccountPk;
	}
	
	/**
	 * 
	 * @param string $userId
	 */
	public function disableGoogleAccount($userId, $googleAccountPk) {
		$this->database->table(self::TABLE_NAME)
				->where(self::ACCOUNT_GOOGLE_FK, $googleAccountPk)
				->where(self::USER_FK, $userId)
				->update([self::STATE => States::Inactive]);			
	}
	
	/**
	 * 
	 * @param string $idFromGoogle
	 */
	public function enableGoogleAccount($userId, $idFromGoogle) {
		$googleAccountPk = $this->googleAccountTable->getGoogleAccountPkBasedOnGoogleId($idFromGoogle);
		$this->database->table(self::TABLE_NAME)
				->where(self::ACCOUNT_GOOGLE_FK, $googleAccountPk)
				->where(self::USER_FK, $userId)
				->update([self::STATE => States::Active]);
	}
}
