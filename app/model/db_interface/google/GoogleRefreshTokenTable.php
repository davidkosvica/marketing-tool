<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Google;

/**
 * Description of GoogleRefreshTokenTable
 *
 * @author davidkosvica
 */
class GoogleRefreshTokenTable {
	
	const TABLE_NAME = 'google_refresh_token';
	const REFRESH_TOKEN = 'refresh_token';
	const GOOGLE_ACCOUNT_FK = 'id_google_account';
	const PRIMARY_KEY = 'id_refresh_token';
		
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function insertOrUpdateRefreshTokenForAccount($accountId, $refreshToken) {
		$this->database->query('INSERT INTO ' . self::TABLE_NAME, [
			self::GOOGLE_ACCOUNT_FK => $accountId,
			self::REFRESH_TOKEN => $refreshToken
		], 'ON DUPLICATE KEY UPDATE', [
			self::REFRESH_TOKEN => $refreshToken
		]);
	}
	
	public function getGoogleRefreshToken($googleAccountPk) {
		/* @var $googleRefreshTokenRow \Nette\Database\Table\ActiveRow */
		$googleRefreshTokenRow = $this->database
				->table(self::TABLE_NAME)
				->where(self::GOOGLE_ACCOUNT_FK, $googleAccountPk)
				->fetch();
		if ($googleRefreshTokenRow) {
			$result = $googleRefreshTokenRow->{self::REFRESH_TOKEN};
		} else {
			$result = false;
		}
		return $result;
	}
}
