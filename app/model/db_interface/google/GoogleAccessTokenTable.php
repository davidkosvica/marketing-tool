<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Google;
use Nette;

/**
 * Description of GoogleAccessTokenTable
 *
 * @author davidkosvica
 */
class GoogleAccessTokenTable {
	
	const TABLE_NAME = 'google_access_token';
	const GOOGLE_ACCOUNT_FK = 'id_google_account';
	const ACCESS_TOKEN = 'access_token';
	const EXPIRES_DATE = 'expires_in';
	const TOKEN_TYPE = 'token_type';
	const CREATED_DATE = 'created';
	const PRIMARY_KEY = 'id_google_access_token';
	const GOOGLE_TOKEN_ID = 'id_token';
		
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var \App\Model\DbInterface\Google\GoogleRefreshTokenTable */
	private $googleRefreshTokenTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->googleRefreshTokenTable = new GoogleRefreshTokenTable($database);
	}
	
	// $accountId -- id_google_account z tabulky google_account
	public function insertOrUpdateAccessToken($googleAccountPk, $accessToken) {
		// vlozim access token, pripadne pokud jiz existuje pouze provedu
		// update jiz existujiciho radku access tokenu
		$this->database->query('INSERT INTO ' . self::TABLE_NAME, [
			self::ACCESS_TOKEN => $accessToken['access_token'],
			self::EXPIRES_DATE => $accessToken['expires_in'],
			self::TOKEN_TYPE => $accessToken['token_type'],
			self::CREATED_DATE => $accessToken['created'],
			self::GOOGLE_ACCOUNT_FK => $googleAccountPk, 
			], 'ON DUPLICATE KEY UPDATE', [ 
				self::ACCESS_TOKEN => $accessToken['access_token'],
				self::EXPIRES_DATE => $accessToken['expires_in'],
				self::CREATED_DATE => $accessToken['created'],
			]);
	}
	
	public function getAccessToken($idGoogleAccount) {
		$result = $this->database
				->table(self::TABLE_NAME)
				->where(self::GOOGLE_ACCOUNT_FK, $idGoogleAccount)
				->fetch();
//		if ($result) {
//			$result = $result->{self::ACCESS_TOKEN};
//		}
		
		return $result->toArray();
	}
}
