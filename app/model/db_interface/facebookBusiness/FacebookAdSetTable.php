<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;
use App\Model\DbInterface\FacebookBusiness\FacebookCampaignTable;
use Nette;
/**
 * Description of facebook_ad_set
 *
 * @author davidkosvica
 */
class FacebookAdSetTable {
	
	const TABLE_NAME = 'facebook_ad_set';
	const START = 'start_time';
	const END = 'end_time';
	const BUDGET = 'daily_budget';
	const LIFETIME_BUDGET = 'lifetime_budget';
	const ADSET_NAME = 'name';
	const CAMPAIGN_FK = 'id_facebook_campaign';
	const FACEBOOK_ID = 'adset_id';
	const STATUS = 'status';
	const PRIMARY_KEY = 'id_facebook_ad_set';
	
	/** @var Nette\Database\Context */
	private $database;
	
	/**
	 * @var App\Model\DbInterface\FacebookBusiness\FacebookCampaignTable
	 */
	private $facebookCampaignTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->facebookCampaignTable = new FacebookCampaignTable($database);
	}
	
	function insertAdSet($adSetData) {
		$campaignPk = $this->facebookCampaignTable
			->getCampaignPkBasedOnFbId($adSetData['campaign_id']);
		
		$this->database->table(self::TABLE_NAME)
				->insert([
					self::START => $adSetData['start_time'],
					self::END => $adSetData['end_time'],
					self::BUDGET => $adSetData['daily_budget'],
					self::LIFETIME_BUDGET => $adSetData['lifetime_budget'],
					self::ADSET_NAME => $adSetData['name'],
					self::FACEBOOK_ID => $adSetData['id'],
					self::STATUS => $adSetData['status'],
					self::CAMPAIGN_FK => $campaignPk 
				]);
	}
	
	function updateAdSet($adSetData) {
		$this->database->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $adSetData['id'])
			->update([
				self::START => $adSetData['start_time'],
				self::END => $adSetData['end_time'],
				self::BUDGET => $adSetData['daily_budget'],
				self::STATUS => $adSetData['status'],
				self::LIFETIME_BUDGET => $adSetData['lifetime_budget']
			]);
	}
	
	function getAdSetPkBaseOnFbId($fbAdSetId) {
		return $this->database->table('facebook_ad_set')
			->where(self::FACEBOOK_ID, $fbAdSetId)
			->select(self::PRIMARY_KEY)
			->fetch()->{self::PRIMARY_KEY};

	}	
}
