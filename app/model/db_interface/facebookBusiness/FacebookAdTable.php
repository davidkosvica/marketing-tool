<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;
use App\Model\DbInterface\FacebookBusiness\FacebookAdSetTable;

/**
 * Description of facebookBusinessAdTable
 *
 * @author davidkosvica
 */
class FacebookAdTable {
	
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var App\Model\DbInterface\FacebookBusiness\FacebookAdSetTable */
	private $adSetTable;
	
	const TABLE_NAME = 'facebook_ad';
	const PRIMARY_KEY = 'id_facebook_ad';
	const CONFIGURED_STATUS = 'configured_status';
	const EFFECTIVE_STATUS = 'effective_status';
	const FACEBOOK_ID = 'ad_id';
	const AD_NAME = 'name';
	const ADSET_FK = 'id_facebook_ad_set';
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	function insertAd($adData) {
		$adSetPk = $this->database->table(FacebookAdSetTable::TABLE_NAME)
			->where(FacebookAdSetTable::FACEBOOK_ID, $adData['adset_id'])
			->select(FacebookAdSetTable::PRIMARY_KEY)
			->fetch()->{FacebookAdSetTable::PRIMARY_KEY};
			
		$this->database->table(self::TABLE_NAME)->insert([
				self::FACEBOOK_ID => $adData['id'],
				self::CONFIGURED_STATUS => $adData['configured_status'],
				self::EFFECTIVE_STATUS => $adData['effective_status'],
				self::AD_NAME => $adData['name'],
				self::ADSET_FK => $adSetPk
			]);
	}
	
	function updateAd($adData) {
		$this->database->table(self::TABLE_NAME)->update([
				self::CONFIGURED_STATUS => $adData['configured_status'],
				self::EFFECTIVE_STATUS => $adData['effective_status'],
			]);
	}
}
