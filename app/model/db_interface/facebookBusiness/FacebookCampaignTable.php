<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;
use App\Model\DbInterface\FacebookBusiness\FacebookAdAccountTable;
use Nette;

/**
 * Description of FacebookCampaignTable
 *
 * @author davidkosvica
 */
class FacebookCampaignTable {
	const TABLE_NAME = 'facebook_campaign';
	const FACEBOOK_ID = 'campaign_id';
	const PRIMARY_KEY = 'id_facebook_campaign';
	const CAMPAIGN_NAME = 'name';
	const CAMPAIGN_OBJECTIVE = 'objective';
	const AD_ACCOUNT_FK = "id_facebook_ad_account";
	
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var App\Model\DbInterface\FacebookBusiness\FacebookAdAccountTable */
	private $facebookAdAccountTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->facebookAdAccountTable = new FacebookAdAccountTable($database);
	}
	
	function getCampaignPkBasedOnFbId($facebookId) {
		$campaignPk = $this->database->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $facebookId)
			->select(self::PRIMARY_KEY)
			->fetch();
		return $campaignPk;
	}
	
	function insertCampaign($campaignData, $adAccount) {
		$adAccountPk = $this->facebookAdAccountTable
			->getAdAccountPkBaseOnFbId($adAccount);
		$this->database->table(self::TABLE_NAME)
				->insert([
					self::FACEBOOK_ID => $campaignData['id'],
					self::CAMPAIGN_NAME => $campaignData['name'],
					self::CAMPAIGN_OBJECTIVE => $campaignData['objective'],
					self::AD_ACCOUNT_FK => $adAccountPk
				]);
	}
}
