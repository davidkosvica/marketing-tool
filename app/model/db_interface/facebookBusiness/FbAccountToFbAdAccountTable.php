<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;
use Nette;

/**
 * Description of FbAccountToFbAdAccountTable
 *
 * @author davidkosvica
 */
class FbAccountToFbAdAccountTable {
	/** 
	 *
	 * @var Nette\Database\Context
	 */
	private $database;
	
	const TABLE_NAME = 'facebook_account_to_ad_account';
	const FACEBOOK_ACCOUNT_FK = 'id_facebook_account';
	const FACEBOOK_AD_ACCOUNT_FK = 'id_facebook_ad_account';
	
	function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function insertRelationship($accountPk, $adAccountPk) {
		$this->database->table(self::TABLE_NAME)
				->insert([
					self::FACEBOOK_ACCOUNT_FK => $accountPk,
					self::FACEBOOK_AD_ACCOUNT_FK => $adAccountPk
				]);
		
	}
	
	function getPrimaryKeysOfAdAccountsOfFbAccount($fbAccountPk) {
		$rows = $this->database->table(self::TABLE_NAME)
				->where(self::FACEBOOK_ACCOUNT_FK, $fbAccountPk)
				->select(self::FACEBOOK_AD_ACCOUNT_FK)
				->fetchAll();
		$primaryKeys = [];
		foreach ($rows as $row) {
			$primaryKeys[] = $row->{self::FACEBOOK_AD_ACCOUNT_FK};
		}
		return $primaryKeys;
	}
}
