<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;
use Nette;
use App\Model\DbInterface\FacebookBusiness\FbAccountToFbAdAccountTable;
use App\Model\DbInterface\Facebook\FacebookAccountTable;

/**
 * Description of FacebookAdAccountTable
 *
 * @author davidkosvica
 */
class FacebookAdAccountTable {
	
	const TABLE_NAME = 'facebook_ad_account';
	const PRIMARY_KEY = 'id_facebook_ad_account';
	const FACEBOOK_ID = 'ad_account_id';
	
	/** @var Nette\Database\Context */
	private $database;
	
	/** @var App\Model\DbInterface\FacebookBusiness\FbAccountToFbAdAccountTable */
	private $fbAccountToFbAdAccountTable;
	
	/** @var App\Model\DbInterface\Facebook\FacebookAccountTable */
	private $facebookAccountTable;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
		$this->fbAccountToFbAdAccountTable = new FbAccountToFbAdAccountTable($database);
		$this->facebookAccountTable = new FacebookAccountTable($database);
	}
	
	public function insertAdAccount($adAccountId, $accountPrimaryKey) {
		$adAccountprimaryKey = $this->database
			->table(self::TABLE_NAME)->insert([
					self::FACEBOOK_ID => $adAccountId
				])->{self::PRIMARY_KEY};
		$this->fbAccountToFbAdAccountTable
			->insertRelationship($accountPrimaryKey, $adAccountprimaryKey);
	}
	
	public function getAdAccountPkBaseOnFbId($adAccountFbId) {
		return $this->database->table(self::TABLE_NAME)
			->where(self::FACEBOOK_ID, $adAccountFbId)
			->select(self::PRIMARY_KEY)
			->fetch()->{self::PRIMARY_KEY};
	}
    
    public function getAccountBaseOnPk(int $primaryKey) {
        $activeRow = $this->database->table(self::TABLE_NAME)
                ->where(self::PRIMARY_KEY, $primaryKey)
                ->fetch();
        if ($activeRow) {
            $result = $activeRow->{self::FACEBOOK_ID};
        } else {
            $result = false;
        }
        return $result;
    }
}
