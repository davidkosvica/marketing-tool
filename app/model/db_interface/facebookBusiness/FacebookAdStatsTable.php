<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;

use App\Model\DbInterface\FacebookBusiness\FacebookAdTable;

/**
 * Description of FacebookAdStatsTable
 *
 * @author david
 */
class FacebookAdStatsTable {
	
	const TABLE_NAME = 'facebook_ad_stats';
	const PK = 'id_facebook_ad_stats';
	const FK = 'id_facebook_ad';
	const CPC = 'CPC';
	const CTR = 'CTR';
	const ACTIONS = 'actions';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	function insertStats($statsValues) {
		$adSetPk = $this->database->table(FacebookAdTable::TABLE_NAME)
			->where(FacebookAdTable::FACEBOOK_ID, $statsValues['campaignId'])
			->select(FacebookAdTable::PRIMARY_KEY)
			->fetch()->{FacebookAdTable::PRIMARY_KEY};
			
		$this->database->table(self::TABLE_NAME)->insert([
				self::CPC => $statsValues[''],
				self::CTR => $statsValues[''],
				self::FK => $adSetPk
			]);
	}
}
