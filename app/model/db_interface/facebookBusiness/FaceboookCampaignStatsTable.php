<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\FacebookBusiness;

use App\Model\DbInterface\FacebookBusiness\FacebookCampaignTable;

/**
 * Description of FaceboookCampaignStatsTable
 *
 * @author david
 */
class FaceboookCampaignStatsTable {
	
	const TABLE_NAME = 'facebook_campaign_stats';
	const PK = 'id_facebook_campaign_stats';
	const FK = 'id_facebook_campaign';
	const CPC = 'CPC';
	const CTR = 'CTR';
	const ACTIONS = 'actions';
	
	/** @var Nette\Database\Context */
	private $database;
	
	function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	function insertStats($statsValues) {
		$campaignPk = $this->database->table(FacebookCampaignTable::TABLE_NAME)
			->where(FacebookCampaignTable::FACEBOOK_ID, $statsValues['campaignId'])
			->select(FacebookCampaignTable::PRIMARY_KEY)
			->fetch()->{FacebookCampaignTable::PRIMARY_KEY};
			
		$this->database->table(self::TABLE_NAME)->insert([
				self::CPC => $statsValues[''],
				self::CTR => $statsValues[''],
				self::FK => $campaignPk
			]);
	}
}
