<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use Nette\Database\UniqueConstraintViolationException;

/**
 * Description of SklikGroupTable
 *
 * @author dave
 */
class SklikGroupTable {
    
    const
		/* Table column names */
		TABLE_NAME = 'sklik_group',
		PK = 'id_group',
		STATUS = 'status',
		NAME = 'name',
		DELETED = 'deleted',
		CREATE_DATE = 'createDate',
		CPC = 'cpc',
		MAX_USER_DAILY_IMPRESSION = 'maxUserDailyImpression',
		CPT = 'cpt',
		ID_FROM_SKLIK = 'id_from_sklik';
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
    
    function insert($group) {
        $timestamp = $group['createDate']->{'timestamp'};
		$datetime = date('Y-m-d H:i:s', $timestamp);
		
		$rowData = [
            self::NAME => $group['name'],
            self::ID_FROM_SKLIK => $group['id'],
            self::DELETED => $group['deleted'],
            self::CREATE_DATE => $datetime,
            self::STATUS => $group['status'],
            self::CPC => $group['cpc'],
            self::CPT => $group['cpt'],
            self::MAX_USER_DAILY_IMPRESSION => $group['maxUserDailyImpression']
        ];
		 
        try {
            $row = $this->database->table(self::TABLE_NAME)->insert($rowData);
            $result = $row->{self::PK};
        } catch (UniqueConstraintViolationException $e) {
            $row = $this->database->table(self::TABLE_NAME)
                    ->where(self::ID_FROM_SKLIK, $rowData['id_from_sklik'])
                    ->update($rowData);
            $result = $this->database->table(self::TABLE_NAME)
                    ->where(self::ID_FROM_SKLIK, $rowData['id_from_sklik'])
                    ->select(self::PK)->fetch()->{self::PK};
        }
        
        return $result;
    }
	
	public function getCreationDate($idGroup) {
		$row = $this->database->table(self::TABLE_NAME)
				->where(self::ID_FROM_SKLIK, $idGroup)
				->select(self::CREATE_DATE)->fetch();
		if ($row) {
			$result = $row->{self::CREATE_DATE};
		} else {
			$result = null;
		}
		return $result;
	}
	
	public function getPk(int $idFromSklik) {
		$row = $this->database->table(self::TABLE_NAME)
				->where(self::ID_FROM_SKLIK, $idFromSklik)
				->select(self::PK)->fetch();
		if ($row) {
			$result = $row->{self::PK};
		} else {
			$result = null;
		}
		
		return $result;
	}
}
