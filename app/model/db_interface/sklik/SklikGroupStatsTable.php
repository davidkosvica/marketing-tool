<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use App\Model\DbInterface\Sklik\SharedElements;
use Nette\Database\UniqueConstraintViolationException;
use App\Model\Sklik\XmlRpcKeywords\Attribute;

/**
 * Description of SklikGroupStatsTable
 *
 * @author dave
 */
class SklikGroupStatsTable {
	
	const 
		TABLE_NAME = 'sklik_group_stats',
		PK = 'id_sklik_group_statistics',
		GROUP_FK = 'id_group';

	/** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	function insert($reports, $groupPk) {
		$query = "INSERT INTO " . self::TABLE_NAME . " ";
		$query .= "(";
		foreach ($reports[0] as $key => $value) {
			$query .= $key . ',';
		}
		$query .= self::GROUP_FK;
		$query .= ") ";
		$query .= "VALUES ";
		foreach ($reports as $report) {
			$query .= "(";
			foreach ($report as $key => $value) {
				if ($key == Attribute::DATE	) {
					$query .= "'" . SharedElements::reformatDatetime($value) . "'";
					$query .= ',';
				} else {
					$query .= $value . ',';
				}
			}
			$query .= $groupPk;
			$query .= "),";
		}
		$query = rtrim($query,",");
		

		try {
			$this->database->query($query);
		} catch(UniqueConstraintViolationException $e) {}
	}
	
	public function getNumRows() {
		return $this->database->table(self::TABLE_NAME)->count();
	}
}
