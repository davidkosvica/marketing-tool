<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use Nette\Database\Context;
use App\Model\DbInterface\Sklik\SharedElements;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use Nette\Database\UniqueConstraintViolationException;

/**
 * Description of SklikKeywordsStatsTable
 *
 * @author dave
 */
class SklikKeywordStatsTable {
	
	const 
		TABLE_NAME = 'sklik_keyword_stats',
		ID_SKLIK_KEYWORD_STAT = 'id_sklik_keyword_stat',
		KEYWORD_FK = 'id_keyword';
	
	/**
	 * @var Context
	 */
	private $database;
	
	public function __construct(Context $context) {
		$this->database = $context;
	}
	
	function insert($reports, $keywordPk) {
		$query = "INSERT INTO " . self::TABLE_NAME . " ";
		$query .= "(";
		foreach ($reports[0] as $key => $value) {
			$query .= $key . ',';
		}
		$query .= self::KEYWORD_FK;
		$query .= ") ";
		$query .= "VALUES ";
		foreach ($reports as $report) {
			$query .= "(";
			foreach ($report as $key => $value) {
				if ($key == Attribute::DATE	) {
					$query .= "'" . SharedElements::reformatDatetime($value) . "'";
					$query .= ',';
				} else {
					$query .= $value . ',';
				}
			}
			$query .= $keywordPk;
			$query .= "),";
		}
		$query = rtrim($query,",");
		

		try {
			$this->database->query($query);
		} catch(UniqueConstraintViolationException $e) {}		
		
	}
	
	public function getNumRows() {
		return $this->database->table(self::TABLE_NAME)->count();
	}
}
