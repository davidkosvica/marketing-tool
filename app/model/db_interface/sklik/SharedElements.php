<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
/**
 * Description of SklikSharedTableOptions
 *
 * @author dave
 */
class SharedElements {
	const
		DATE_FORMAT = 'Y-m-d H:i:s',
		DATE = 'date',
		IMPRESSIONS = 'impressions',
		CLICKS = 'clicks',
		CTR = 'ctr',
		CPC = 'cpc',
		PRICE = 'price',
		AVG_POSITION = 'avgPosition',
		CONVERSIONS = 'conversions',
		CONVERSION_RATIO = 'conversionRatio',
		CONVERSION_AVG_PRICE = 'conversionAvgPrice',
		CONVERSION_VALUE = 'conversionValue',
		CONVERSION_AVG_VALUE = 'conversionAvgValue',
		CONVERSION_VALUE_RATIO = 'conversionValueRatio',
		TRANSACTIONS = 'transactions',
		TRANSACTION_AVG_PRICE = 'transactionAvgPrice',
		TRANSACTION_AVG_VALUE = 'transactionAvgValue',
		TRANSACTION_AVG_COUNT = 'transactionAvgCount',
		CREATE_DATE = 'createDate';
	
	static function reformatDatetime($datetimeObject) {
		$dateString = $datetimeObject->{Attribute::SCALAR};
		$dateInterface = date_create($dateString);
		return date_format($dateInterface, SharedElements::DATE_FORMAT); 
	}
	
	static function clearData($dataSets, array $toBeDeleted) {
		foreach ($dataSets as $dataSet) {
			foreach($dataSet as $key => $value) {
				if (in_array($key, $toBeDeleted)) {
					unset($dataSet[$key]);
				}
			}
			$cleared[] = $dataSet;
		}

		return $cleared;
	}
	
	static function formatValue($value) {
		if ($value === false) {
			$formattedValue = 'FALSE';
		} elseif ($value === true) {
			$formattedValue = 'TRUE';
		} elseif ($value === NULL) {
			$formattedValue = 'NULL';
		} elseif (is_string($value)) {
			$formattedValue = "'{$value}'";
		} else {
			$formattedValue = $value;
		}
		
		return $formattedValue;
	}
}
