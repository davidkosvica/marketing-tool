<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use App\Model\DbInterface\Sklik\SharedElements;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use Nette\Database\UniqueConstraintViolationException;

/**
 * Description of SklikCampaignStatsTable
 *
 * @author dave
 */
class SklikCampaignStatsTable {
	
	const
        TABLE_NAME = 'sklik_campaign_stats',
		PK = 'id_statistics',
		CAMPAIGN_FK = 'id_campaign';
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	function insert($reports, $campaignId) {
		$query = "INSERT INTO " . self::TABLE_NAME . " ";
		$query .= "(";
		foreach ($reports[0] as $key => $value) {
			$query .= $key . ',';
		}
		$query .= self::CAMPAIGN_FK;
		$query .= ") ";
		$query .= "VALUES ";
		foreach ($reports as $report) {
			$query .= "(";
			foreach ($report as $key => $value) {
				if ($key == Attribute::DATE	) {
					$query .= "'" . SharedElements::reformatDatetime($value) . "'";
					$query .= ',';
				} else {
					$query .= $value . ',';
				}
			}
			$query .= $campaignId;
			$query .= "),";
		}
		$query = rtrim($query,",");
		

		try {
			$this->database->query($query);
		} catch(UniqueConstraintViolationException $e) {}
	}
	
	public function getNumRows() {
		return $this->database->table(self::TABLE_NAME)->count();
	}
}
