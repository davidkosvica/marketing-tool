<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use Nette\Database\UniqueConstraintViolationException;
use App\Model\Sklik\SklikXML;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
/**
 * Description of SklikCampaignTable
 *
 * @author david
 */
class SklikCampaignTable {
	 
    const
	/* Table column names */
        TABLE_NAME = 'sklik_campaign',
        PK = 'id_campaign',
        SKLIK_ACC_FK = 'id_account',
        ID_FROM_SKLIK = 'id_from_sklik',
        CAMPAIGN_NAME = 'name',
        CAMPAIGN_DATE = 'date',
        CAMPAIGN_STATUS = 'status',
	
	/* Sklik API names */
		STATUS = 'status',
		ID = 'id',
		NAME = 'name',
		DATE = 'date';
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
    
    function insert($campaign_attributes, int $sklikAccountPk) {
		$dateString = $campaign_attributes[Attribute::CREATE_DATE]
				->{Attribute::SCALAR};
		$dateInterface = date_create($dateString);
		$date = date_format($dateInterface, 'Y-m-d H:i:s'); 
		
		try {
			$this->database->table(self::TABLE_NAME)
				->insert([
					self::CAMPAIGN_DATE => $date,
					self::ID_FROM_SKLIK => $campaign_attributes[self::ID],
					self::CAMPAIGN_NAME => $campaign_attributes[self::NAME],
					self::CAMPAIGN_STATUS => $campaign_attributes[self::STATUS],
					self::SKLIK_ACC_FK => $sklikAccountPk
				]);
		} catch (UniqueConstraintViolationException $e) {
			
		}
	}
    
    function getPk(int $idFromSklik) {
        $result = $this->database->table(self::TABLE_NAME)
                ->select(self::PK)
                ->where(self::ID_FROM_SKLIK, $idFromSklik)
                ->fetch();
        if ($result) {
           $result = $result->{self::PK}; 
        } else {
           $result = null; 
        }
        return $result;
    }
	
	
	public function getCreationDate($idCampaign) {
		$row = $this->database->table(self::TABLE_NAME)
				->where(self::ID_FROM_SKLIK, $idCampaign)
				->select(self::DATE)->fetch();
		if ($row) {
			$result = $row->{self::DATE};
		} else {
			$result = null;
		}
		return $result;
	}
}
