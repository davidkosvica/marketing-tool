<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use Nette\Database\Table\ActiveRow;
use Nette;

/**
 * Description of sklikUserTable
 *
 * @author david
 */
class SklikAccountTable {
    
    const
        TABLE_NAME = 'sklik_account',
        PK = 'id_account',
        SKLIK_ID_USER = 'id_user_from_sklik',
        SESSION = 'session';
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
    
    function getSession(int $primaryKey) {
        $activeRow = $this->database->table(self::TABLE_NAME)
                ->where(self::PK, $primaryKey)
                ->select(self::SESSION)->fetch();
        if ($activeRow) {
            $result = $activeRow->{self::SESSION};
        } else {
            $result = false;
        }
        return $result;
    }
	
	function getSklikUsrId(int $primaryKey) {
        $activeRow = $this->database->table(self::TABLE_NAME)
                ->where(self::PK, $primaryKey)
                ->select(self::SKLIK_ID_USER)->fetch();
        if ($activeRow) {
            $result = $activeRow->{self::SKLIK_ID_USER};
        } else {
            $result = false;
        }
        return $result;
    }
    
    function insert(array $accountAttributes) {
        /* @var $activeRow ActiveRow */
        try {
            $activeRow = $this->database->table(self::TABLE_NAME)
                ->insert([
                    self::SKLIK_ID_USER => $accountAttributes['userId'],
                    self::SESSION => $accountAttributes['session']
                ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            $this->database->table(self::TABLE_NAME)
                ->where(self::SKLIK_ID_USER, $accountAttributes['userId'])
                ->update([
                    self::SESSION => $accountAttributes['session']
                ]);
            $activeRow = $this->database
                    ->table(self::TABLE_NAME)
                    ->where(self::SKLIK_ID_USER, $accountAttributes['userId'])
                    ->fetch();
        }
        
        return $activeRow->{self::PK};
    }
    
//    function setSession(string $session, int $sklikId) {
    function setSession(string $session, int $sklikAccountPk) {
        return $this->database->table(self::TABLE_NAME)
                ->where(self::PK, $sklikAccountPk)
                ->update([
                    self::SESSION => $session
                ]);
    }
}
