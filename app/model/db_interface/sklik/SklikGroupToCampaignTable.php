<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use \Nette\Database\Context;
use \Nette\Database\ForeignKeyConstraintViolationException;

/**
 * Description of SklikGroupToCampaignTable
 *
 * @author dave
 */
class SklikGroupToCampaignTable {
    const
	/* Table column names */
        TABLE_NAME = 'sklik_group_to_campaign',
        FK_GROUP = 'id_group',
        FK_CAMPAIGN = 'id_campaign';
	
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(Context $database) {
		$this->database = $database;
	}
    
    function insert($groupPk, $campaignPk) {
        $data = [
            self::FK_GROUP => $groupPk, 
            self::FK_CAMPAIGN => $campaignPk
        ];
        try {
            $row = $this->database->table(self::TABLE_NAME)
                ->insert($data);
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $row = $this->database->table(self::TABLE_NAME)
                    ->select(self::FK_GROUP)
                    ->select(self::FK_CAMPAIGN)->fetch();
        }

        $result[] = $row->{self::FK_GROUP};
        $result[] = $row->{self::FK_CAMPAIGN};
        
        return $result;
    }
    
    
}
