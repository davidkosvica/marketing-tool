<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use App\Model\DbInterface\States;
use Nette;

/**
 * Description of UserToSklikAccountTable
 *
 * @author david
 */
class UserToSklikAccountTable {
    const
        USER_FK = 'id_user',
        SKLIK_ACCOUNT_FK = 'id_account',
        TABLE_NAME = 'user_to_sklik_account',
        STATES = 'state';
    
    /** @var Nette\Database\Context */
	private $database;
    
    function __construct(\Nette\Database\Context $database) {
		$this->database = $database;
	}
    
    public function insert($sklikAccountPk, $userId) {
        try {
            $activeRow = $this->database->table(self::TABLE_NAME)
                ->insert([
                    self::USER_FK => $userId,
                    self::SKLIK_ACCOUNT_FK => $sklikAccountPk,
                    self::STATES => States::Inactive
                ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {}
    }
    
    public function setActive(int $userId, int $sklikAccountPk) : int {
        $affected = $this->database->table(self::TABLE_NAME)
                ->where(self::USER_FK, $userId)
                ->where(self::SKLIK_ACCOUNT_FK, $sklikAccountPk)
                ->update([self::STATES => States::Active]);
        return $affected;
    }
    
    public function setInactive(int $userId, int $sklikAccountPk) : int {
        $affected = $this->database->table(self::TABLE_NAME)
                ->where(self::USER_FK, $userId)
                ->where(self::SKLIK_ACCOUNT_FK, $sklikAccountPk)
                ->update([self::STATES => States::Inactive]);
        return $affected;
    }

    public function getUsersActiveAccount(int $userId) : int {
        /* @var $row Nette\Database\Table\ActiveRow */
        $row = $this->database
                ->table(self::TABLE_NAME)
                ->where(self::USER_FK, $userId)
                ->where(self::STATES, States::Active)
                ->fetch();
        if ($row) {
            $result = $row->{self::SKLIK_ACCOUNT_FK};
        } else {
            $result = false;
        }
        return $result;
    }
}
