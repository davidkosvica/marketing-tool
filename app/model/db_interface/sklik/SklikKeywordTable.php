<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\DbInterface\Sklik;
use App\Model\DbInterface\Sklik\SharedElements;
use Nette\Database\UniqueConstraintViolationException;
use App\Model\Sklik\XmlRpcKeywords\Attribute;
use Nette\Database\Context;

/**
 * Description of SklikKeywordsTable
 *
 * @author dave
 */
class SklikKeywordTable {
	
	const 
		TABLE_NAME = 'sklik_keyword',
		PK = 'id_keyword',
		SKLIK_ID = 'id',
		KEYWORD = 'keyword',
		CREATE_DATE = 'createDate',
		GROUP_FK = 'id_group';

	/**
	 * @var Context
	 */
	private $database;
	
	public function __construct(Context $context) {
		$this->database = $context;
	}
	
	public function insert($keywords, $groupPk) {
		$toBeDeleted = [Attribute::GROUP];
		$keywords = SharedElements::clearData($keywords, $toBeDeleted);
		$query = "INSERT INTO " . self::TABLE_NAME . " ";
		$query .= "(";
		$keys = array_keys($keywords[0]);
		
		foreach ($keys as $key) {
			$query .= $key . ',';
		}
		$query .= self::GROUP_FK;
		$query .= ") ";
		
		$query .= "VALUES ";
		foreach ($keywords as $keyword) {
			$query .= "(";
			foreach ($keyword as $key => $value) {
				if ($key == Attribute::CREATE_DATE	) {
					$query .= "'" . SharedElements::reformatDatetime($value) . "'";
					$query .= ',';
				} else {
					$query .= SharedElements::formatValue($value) . ',';
				}
			}
			$query .= $groupPk;
			$query .= "),";
		}
		$query = rtrim($query,",");
		

		try {
			$this->database->query($query);
		} catch(UniqueConstraintViolationException $e) {}
	}
	
	public function getPk($id) {
		$row = $this->database
				->table(self::TABLE_NAME)
				->select(self::PK)
				->where(self::SKLIK_ID, $id)
				->fetch();
		
		if ($row) {
			$pk = $row->{self::PK};
		} else {
			$pk = null;
		}
		return $pk;
	}
	
	public function getCreationDate($idKeyword) {
		$row = $this->database->table(self::TABLE_NAME)
				->where(self::SKLIK_ID, $idKeyword)
				->select(self::CREATE_DATE)->fetch();
		if ($row) {
			$result = $row->{self::CREATE_DATE};
		} else {
			$result = null;
		}
		return $result;
	}
}
