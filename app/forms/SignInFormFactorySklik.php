<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Forms;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use App\Model\Sklik\SklikAdAccounts;
use Kdyby;
use Kdyby\Translation\Translator;
use App\Model\UserManager;
use Nette\Database\Context;

/**
 * Description of SignUpFormFactorySklik
 *
 * @author davidkosvica
 */
class SignInFormFactorySklik {
    use Nette\SmartObject;

	/** @persistent */
	public $locale;

	/** @var Kdyby\Translation\Translator */
	public $translator;
        
	/** @var FormFactory */
	private $factory;

	/** @var App\Model\Sklik\SklikAdAccounts */
	private $sklikAdAccounts;
	
    /** @var User */
	private $user;
    
    /** @var UserManager */
    private $userManager;


	public function __construct(FormFactory $factory, 
            User $user, 
            Translator $translator, 
            UserManager $userManager,
            Context $database)
	{
		$this->factory = $factory;
		$this->user = $user;
		$this->translator = $translator;
		$this->sklikAdAccounts = new SklikAdAccounts($database);
        $this->userManager = $userManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->setTranslator($this->translator);
		$form->addEmail('email', 'email')
			->setRequired('Prosim zadejte seznam email.');
        
		$form->addPassword('password', 'heslo')
			->setRequired('Prosim zadejte heslo k seznam emailu.');

		$form->addSubmit('send', 'Připojit');
		
		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
                $clientAttributes = $this->sklikAdAccounts
                        ->clientLogin($values->email, $values->password);
                $userId = $this->user->getId();
                $link = $this->userManager->insertUsersSklikAccount($clientAttributes, $userId);
                $this->userManager->activateUsersSklikAccount($link['user_id'], $link['sklikAccountPk']);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError('The username or password you entered is incorrect.');
				return;
			}
			$onSuccess();
		};
		
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = null;
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';
	

		return $form;
	}

}
