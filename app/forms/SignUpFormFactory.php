<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;
use Kdyby;

class SignUpFormFactory
{
    use Nette\SmartObject;

    const PASSWORD_MIN_LENGTH = 7;

    /** @persistent */
    public $locale;

    /** @var Kdyby\Translation\Translator */
    public $translator;

    /** @var FormFactory */
    private $factory;

    /** @var Model\UserManager */
    private $userManager;


    public function __construct(FormFactory $factory, 
                                Model\UserManager $userManager, 
                                Kdyby\Translation\Translator $translator)
    {
            $this->factory = $factory;
            $this->translator = $translator;
            $this->userManager = $userManager;
    }


    /**
     * @return Form
     */
    public function create(callable $onSuccess)
    {
        $form = $this->factory->create();
        $form->setTranslator($this->translator);
        $form->addText('username', 'forms.sign.username')
                ->setRequired('forms.sign.username_required_note');

        $form->addEmail('email', 'forms.sign.email')
                ->setRequired('forms.sign.email_required_note')
                ->setDefaultValue('user@example.com');

        $form->addPassword('password', 'forms.sign.password')
                ->setOption('description', sprintf($this->translator->translate(
						"forms.sign.password_length_info", 
						['n' => self::PASSWORD_MIN_LENGTH])))
                ->setRequired('forms.sign.password_required_note')
                ->addRule($form::MIN_LENGTH, null, self::PASSWORD_MIN_LENGTH);

        $form->addPassword('password_check', 'forms.sign.password_check')
                ->setRequired('forms.sign.password_required_check_note');

        $form->addSubmit('send', 'forms.sign.registration_button');
		
        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
                try {
                        $this->userManager->add($values->username,
                                                $values->email,
                                                $values->password,
												$values->password_check);
                } catch (Model\DbInterface\DuplicateNameException $e) {
                        $form['username']->addError('forms.sign.username_error');
                        return;
                } catch (Model\DbInterface\PasswordsDoNotMatchException $e) {
                        $form['password_check']->addError(
							'forms.sign.password_check_error');
                        return;
                }
                $onSuccess();
        };
		
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = null;
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';
		
        return $form;
    }
}
