<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Kdyby;

class SignInFormFactory
{
	use Nette\SmartObject;

    /** @persistent */
	public $locale;

	/** @var Kdyby\Translation\Translator */
	public $translator;
        
	/** @var FormFactory */
	private $factory;

	/** @var User */
	private $user;


	public function __construct(FormFactory $factory,
                                    User $user,
                                    Kdyby\Translation\Translator $translator)
	{
		$this->factory = $factory;
		$this->user = $user;
		$this->translator = $translator;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->setTranslator($this->translator);
		$form->addText('username', 'forms.sign.username')
			->setRequired('forms.sign.username_required_note');

		$form->addPassword('password', 'forms.sign.password')
			->setRequired('forms.sign.password_required_note');
                
		$form->addCheckbox('remember', 'Pamatuj si přihlášení');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->username, $values->password);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError('The username or password '
					. 'you entered is incorrect.');
				return;
			}
			$onSuccess();
		};
		
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = null;
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn btn-default';

		return $form;
	}
}
