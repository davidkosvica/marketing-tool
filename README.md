Prototype of Marketing Tool for my bachelor thesis build on Nette project. 
It connects to Sklik API, Facebook Ads API, Google Adwords API. 
As for now it is a basic backend platform that downloads Campaign structures from connected
APIs and then it stores them into local DB, it then downloads desired statistics about their performance.

1. goal is to rewrite Adwords, Facebook Ads Managers so they properly uses Dependency Injection.
2. create simple form that will populate services with campaigns, that will specific atributes (same name for example, necessary for utm_tags)
3. goal is to finish Google Analytics Manager.
	 Implement feature that will through use of utm_tags analyse incoming trafic.
	The goal of the analysis is to compare traffic from separate marketing services (sklik vs adwords vs facebook)
	Find out which marketing service drives bigger trafic to a given website, leads to more conversion etc.

	

Main idea behind the app is that it will serve to internet marketers by giving them better insight
by comparing the traffic and conversions between third party services (Sklik, Facebook Ads, Adwords).